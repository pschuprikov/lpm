{ lib, buildPythonPackage, classifiers_lib, click }:
let
in buildPythonPackage {
  pname = "testing";

  version = "0.1dev";

  src = /home/pschuprikov/repos/lpm/testing/.;

  propagatedBuildInputs = [ classifiers_lib click ];
}
