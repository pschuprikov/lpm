self: super: {
  classifiers_lib =
    super.python3Packages.callPackage ./classifiers-lib.nix { 
      llvmPackages = super.llvmPackages_10;
    };
  myrunner = self.python3Packages.callPackage ./testing.nix { };
}
