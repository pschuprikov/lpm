{
  network.description = "LPM algorithm evaluation";

  defaults = { config, pkgs, ... }:
    let
      classifiers_lib =
        pkgs.python3Packages.callPackage ./classifiers-lib.nix { 
          llvmPackages = pkgs.llvmPackages_10;
        };
      myrunner = pkgs.python3Packages.callPackage ./testing.nix {
        inherit classifiers_lib;
      };
    in {
      networking.hostName = "lpm-evals";
      programs.tmux.enable = true;
      environment.systemPackages = [ myrunner pkgs.rxvt_unicode.terminfo ];
    };
}
