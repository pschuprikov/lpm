{ lib, llvmPackages, numpy, toPythonModule, buildPythonPackage, python, cmake
, spdlog, boost, pytest, wrapCCWith, tbb, gcc10, catch2 }:
let
  gccForLibs = gcc10.cc;
  wrapCCWithGcc10 = args: wrapCCWith (args // { inherit gccForLibs; } );
  llvmPackagesGcc10 = llvmPackages.override { 
    buildLlvmTools = llvmPackagesGcc10.tools;
    targetLlvmLibraries = llvmPackages.libraries;
    wrapCCWith = wrapCCWithGcc10;
    inherit gccForLibs;
  };

  boost_with_numpy = boost.override {
    enablePython = true;
    enableNumpy = true;
    python = python;
  };

  p4t_native = toPythonModule (llvmPackagesGcc10.stdenv.mkDerivation {
    name = "p4t-native";
    src = builtins.fetchGit {
      url = /home/pschuprikov/repos/classifiers-lib;
    };

    sourceRoot = "source/p4t_native";

    nativeBuildInputs = [ cmake ];

    buildInputs = [ spdlog catch2 boost_with_numpy tbb python ];

    doCheck = true;

    cmakeFlags = [ "-DPYTHON_SITE_PACKAGES_DIR=${python.sitePackages}" ];
  });
in buildPythonPackage {
  pname = "classifiers-lib";

  version = "0.0.1";

  src = builtins.fetchGit {
    url = /home/pschuprikov/repos/classifiers-lib/.;
  };

  checkPhase = ''
    python -m pytest
  '';

  buildInputs = [ pytest ];

  propagatedBuildInputs = [ p4t_native numpy ];

}
