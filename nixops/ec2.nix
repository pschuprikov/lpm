{ pkgs ? import <nixpkgs> { }, access-key, key-pair, region }:
let
  createMachine = instanceType:
    { config, pkgs, ... }: {
      imports = [ <nixpkgs/nixos/modules/virtualisation/amazon-image.nix> ];
      ec2.hvm = true;
      boot.loader.grub.device = pkgs.lib.mkForce "/dev/nvme0n1";
      deployment.targetEnv = "ec2";
      deployment.ec2.accessKeyId = access-key;
      deployment.ec2.region = "us-east-2";
      deployment.ec2.instanceType = instanceType;
      deployment.ec2.keyPair = key-pair;
      deployment.ec2.privateKey = "${builtins.getEnv "HOME"}/.aws/${key-pair}.pem";
      deployment.ec2.securityGroups = [ "ssh-only" ];
      deployment.ec2.ebsInitialRootDiskSize = 30;
    };
  #baseType = "c5";
  baseType = "m5";
  subtypes = {
    "4" = "xlarge";
    "8" = "2xlarge";
    "2" = "large";
  };
  createMachineNCores = ncores: idx:
    let subtype = subtypes."${toString ncores}";
    in {
      name = "runner${toString ncores}_${toString idx}";
      value = createMachine "${baseType}.${subtype}";
    };
in builtins.listToAttrs (map (idx: createMachineNCores 8 idx) (pkgs.lib.range 1 3))
