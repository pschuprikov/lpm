from __future__ import annotations
from typing import NamedTuple, Optional, Sequence, cast, Iterable
import os.path
import json
import sys
import numpy as np
import matplotlib
from matplotlib.patches import Circle, Patch
from matplotlib.transforms import ScaledTranslation
import matplotlib.pyplot as plt
matplotlib.use("PS")
import pandas as pd

import click


VARIABLE_TO_YLABEL = {
        'tcam_entries': 'Number of TCAM entries',
        'stage_number': 'Number of Pipeline stages',
        }


BASIC_METHOD: str = 'basic'


class BasicPipeInfo(NamedTuple):
    cls: str
    method: str
    width: Optional[int]

    @staticmethod
    def from_string_tuple(t) -> BasicPipeInfo:
        cls, method, width= t
        return BasicPipeInfo(cls, method, int(width) if width != 'None' else None)


class PipeInfo(NamedTuple):
    cls: str
    method: str
    width: Optional[int]
    bound_kind: str
    bound_val: float
    key_method: str
    combiner: str

    @staticmethod
    def from_string_tuple(t) -> PipeInfo:
        cls, method, width, bound_kind, bound_val, key_method, combiner = t
        return PipeInfo(cls, method, int(width) if width != 'None' else None, str(bound_kind), float(bound_val), key_method, combiner)


@click.group()
def main():
    pass


@main.command()
@click.argument('variable',
                type=click.Choice(VARIABLE_TO_YLABEL),
                required=True)
@click.argument('key_method', type=str, required=True)
@click.argument('combiner', type=str, required=True)
@click.argument('stat_file', type=str, required=True)
@click.argument('basic_stat_file', type=str, required=False)
@click.argument('plot_file', type=str, required=True)
def plot(variable, key_method, combiner, stat_file,
         basic_stat_file: Optional[str], plot_file):
    stats: pd.DataFrame = cast(pd.DataFrame,
                               pd.read_csv(stat_file, converters=CONVERTERS))
    if basic_stat_file is not None:
        basic_stats: pd.DataFrame = cast(
            pd.DataFrame, pd.read_csv(basic_stat_file, converters=CONVERTERS))
        stats = pd.concat([stats, basic_stats])
    create_figure(stats, variable, key_method, combiner).savefig(plot_file)


CONVERTERS = {'combiner': lambda s: s, 'error': lambda s: s}


class BarParams(NamedTuple):
    num_groups: int
    width: float = 0.15
    gap: float = 0.05

    @property
    def total_width(self) -> float:
        return self.width * self.num_groups + self.gap * (self.num_groups - 1)

    @property 
    def step(self) -> float:
        return (self.total_width - self.width) / (self.num_groups - 1) if self.num_groups > 1 else 0


def create_figure(stats: pd.DataFrame, variable: str, key_method: str, combiner: str) -> plt.Figure:
    stats.query(f'key_method == "{key_method}" & combiner == "{combiner}" | method == "{BASIC_METHOD}"', inplace=True)
    classifiers = stats['cls'].unique()
    num_classifiers = len(classifiers)
    num_groups = len(stats['method'].unique())
    bar_params = BarParams(num_groups)
    max_value = cast(pd.DataFrame, stats.query(f'method != "{BASIC_METHOD}"'))[variable].max()

    matplotlib.rcParams["font.family"] = "serif"
    matplotlib.rcParams["font.size"] = "10"
    matplotlib.rcParams["hatch.linewidth"] = 0.5

    x = np.arange(num_classifiers)

    methods = stats['method'].unique()
    print('methods:', *methods)


    fig, ax = plt.subplots(figsize=(516/72, 1.5), gridspec_kw=dict(left=0.1, top=0.85, bottom=0.25, right=0.95))
    for i, name in enumerate(methods):
        draw(fig, ax, i, x, stats.query(f'method == "{name}"'), variable, name, BarParams(num_groups))
    ax.set_xlabel("Classifier name", fontsize='x-small')
    ax.set_ylabel(VARIABLE_TO_YLABEL[variable], fontsize='x-small')
    ax.set_xticks(x)
    ax.set_xticklabels(classifiers, fontweight="bold", fontsize='x-small')
    if not np.isnan(max_value):
        ax.set_yticks(np.arange(0, max_value, 10000))
        ax.set_yticklabels(np.arange(0, max_value, 10000), fontsize='x-small')
        ax.set_ylim((None, max_value * 1.1))
    ax.set_axisbelow(True)
    ax.set_xlim((-(bar_params.total_width + bar_params.width) * 0.5, num_classifiers - 1 + (bar_params.total_width + bar_params.width) * 0.5))
    ax.legend(bbox_to_anchor=(0, 0.9), loc="lower left", ncol=num_groups, frameon=False, handles=[Patch(label=STYLES[m]['label'], **_label_kwargs(m)) for m in methods])
    ax.grid(axis='y')
    return fig


STYLES = {
    "exact": {"color": "white", "hatch": "xxxxx", "label": "EXACT"},
    "lpm_w_oi": {"color": "black", "hatch": None, "label": "PREDICAT"},
    "oi": {"color": "gray", "hatch": None, "label": "SAX-PAC"},
    "oi_lpm_joint": {"color": "white", "hatch": "/////", "label": "LPM-PR"},
    "basic": {"color": "white", "hatch": None, "label": "ORIG"},
}


def draw(fig: plt.Figure, ax: plt.Axes, i: int, x, group: pd.DataFrame, variable: str, name: str, bar_params: BarParams):
        ys = group[variable]
        xs = pd.Series(x - (bar_params.total_width - bar_params.width) * 0.5 + i * bar_params.step, group.index)
        good_ix = ~np.isnan(ys)
        bad_ix = ~good_ix
        ax.bar(xs[good_ix], height=ys[good_ix], width=bar_params.width,
               edgecolor="black", linewidth=0.5, **STYLES[name])
        for ix in bad_ix.index[bad_ix]:
            pt = 1 / 72
            annotation_label = group['error'].at[ix]
            ax.annotate(annotation_label or 'FAIL', xy=(xs.at[ix], 7), 
                        xycoords=('data', 'axes points'),
                        xytext=(xs.at[ix], 20), textcoords=('data', 'axes points'),
                        rotation=80, horizontalalignment='center',
                        arrowprops={'linewidth': 0.5,
                                    'arrowstyle': '->',
                                    'shrinkA': 0, 'shrinkB': 1},
                        fontsize='xx-small', fontfamily='sans')
            ax.add_patch(Circle(
                (0, 3 * pt + 0.5 * pt), 3 * pt,
                linewidth=0.5,
                transform=fig.dpi_scale_trans + ScaledTranslation(xs.at[ix], 0, ax.transData),
                **_label_kwargs(name)))


def _label_kwargs(method: str) -> dict:
    return dict(hatch=STYLES[method]['hatch'], facecolor=STYLES[method]['color'], edgecolor='black')

@main.command()
@click.option('--pipe', nargs=len(PipeInfo._fields) + 1, multiple=True)
@click.option('--basic-pipe', nargs=len(BasicPipeInfo._fields) + 1, multiple=True)
@click.option('--output', required=True)
def analyze(pipe: Sequence[str], basic_pipe: Sequence[str], output: str):
    entries = []
    for pipe_info, manifest_file in pipe_arguments_to_pipes(pipe, basic_pipe):
        entries.append(analyze_pipe(pipe_info, manifest_file))
    pd.DataFrame(entries).to_csv(output, index=False)


def pipe_arguments_to_pipes(
    pipe: Sequence[str], basic_pipe: Sequence[str]
) -> Iterable[tuple[PipeInfo | BasicPipeInfo, str]]:
    yield from ((PipeInfo.from_string_tuple(t), manifest_file)
                for *t, manifest_file in pipe)
    yield from ((BasicPipeInfo.from_string_tuple(t), manifest_file)
                for *t, manifest_file in basic_pipe)


def analyze_pipe(pipe_info: PipeInfo | BasicPipeInfo, manifest_file: str):
    if os.path.getsize(manifest_file) == 0:
        return flatten(EMPTY_RESULT) | pipe_info._asdict()

    with open(manifest_file, 'r') as manifest_in:
        manifest = json.load(manifest_in)

    if 'error' in manifest:
        return process_error_manifest(manifest) | pipe_info._asdict()

    if len(manifest['programs']) > 1:
        print('There must be only one program', file=sys.stderr)
        exit(1)

    if len(manifest['programs'][0]['pipes']) > 1:
        print('There must be only one pipe', file=sys.stderr)
        exit(1)

    json_path = None
    for log in manifest['programs'][0]['pipes'][0]['files']['logs']:
        if os.path.basename(log['path']) == 'mau.json':
            json_path = os.path.join(os.path.dirname(manifest_file), log['path'])

    result = process_pipe(json_path)
    return flatten(result) | pipe_info._asdict()


def process_error_manifest(manifest: dict) -> dict:
    error = manifest['error']
    if error == 'timeout':
        return flatten(EMPTY_RESULT) | {'error': 'TIMEOUT'}
    if 'nonzero-return' in error:
        return flatten(EMPTY_RESULT) | {'error': f"NZR {error['nonzero-return']}"}
    raise AssertionError(f'Unknown error: {error}') 


EMPTY_RESULT = {
        'tcam': {'entries': None,'memories': None},
        'sram': {'entries': None,'memories': None},
        'stage_number': None, 'error': None
        }


def flatten(d: dict) -> dict:
    result = {}
    for k, v in d.items():
        if not isinstance(v, dict):
            result[k] = v
            continue
        for k_, v_ in flatten(v).items():
            result[f'{k}_{k_}'] = v_
    return result

def process_pipe(pipe_json):
    with open(pipe_json) as mau_f:
        mau = json.load(mau_f)

    tcam_entries_requested = 0
    tcam_memories = 0
    sram_entries_requested = 0
    sram_memories = 0
    max_stage_number = 0

    for table in mau['tables']:
        for stage_allocation in table['stage_allocation']:
            max_stage_number = max(max_stage_number, stage_allocation['stage_number'])
            for memory in stage_allocation['memories']:
                if memory['memory_type'] == 'tcam':
                    tcam_entries_requested += memory['entries_requested']
                    tcam_memories += memory['num_memories']
                elif memory['memory_type'] == 'sram' and 'entries_requested' in memory:
                    sram_entries_requested += memory['entries_requested']
                    sram_memories += memory['num_memories']

    return {
        'tcam': {
            'entries': tcam_entries_requested,
            'memories': tcam_memories},
        'sram': {
            'entries': sram_entries_requested,
            'memories': sram_memories},
        'stage_number': max_stage_number,
        'error': None
        }

if __name__ == '__main__':
    main()
