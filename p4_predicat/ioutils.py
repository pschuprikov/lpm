from contextlib import contextmanager
from typing import IO, List

class P4Writer:
    def __init__(self, out: IO):
        self.out = out
        self.current_indent = 0

    def writelines(self, lines: List[str]): 
        self.out.writelines(self._lines(lines))

    @contextmanager
    def block(self, init: str):
        self.writelines([init + ' {'])
        self.current_indent += 1
        try:
            yield
        finally:
            self.current_indent -= 1
            self.writelines(['}'])


    def _lines(self, lines: List[str]) -> List[str]:
        return ['  ' * self.current_indent + f'{line}\n' for line in lines]
