{
  description = "switchd environment";

  inputs.bf-sde-nix.url = "git+file:///home/pschuprikov/repos/bf-sde-nix";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";

  outputs = { self, bf-sde-nix, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      bf_sde_packages =
        bf-sde-nix.legacyPackages.${system}.bf-sde-packages_9_5_1;
      p4_predicat = bf_sde_packages.makeExample {
        name = "p4_predicat";
        path = ".";
        p4-arch = "tna";
        src = pkgs.lib.cleanSourceWith {
          filter = path: type:
            type != "directory" || baseNameOf path != "build" && baseNameOf path
            != "pcap_output";
          src = ./.;
        };
      };
      testEnv = bf_sde_packages.testEnv { example = p4_predicat; };
      compileShell = { name, arch, path ? "../p4", prefix ? null }:
        (bf_sde_packages.makeExample {
          inherit name path;
          p4-arch = arch;
          src = null;
          p4c = "/home/pschuprikov/work/usi/bf-sde-9.9.0/install/bin/bf-p4c";
        }).overrideAttrs (attrs: pkgs.lib.optionalAttrs (prefix != null) { inherit prefix; });
    in {
      devShells.${system} = {
        default = pkgs.mkShell rec {
          buildInputs = [
            (compileShell {
              name = null;
              arch = "t2na";
            }).buildInputs
          ];
          passthru = {
            example = p4_predicat;
            inherit bf_sde_packages compileShell;
          };
        };
      };
    };
}
