let
  flake = import ~/repos/bf-sde-nix;
  pkgs = import <nixpkgs> {};
in flake.lib.makePtfShell {
  otherPythonPackages = ps: with ps; [ click numpy toml pandas matplotlib ];
  otherBuildInputs = [ pkgs.pyright pkgs.yapf ];
  shellHook = ''
    export PYTHONPATH=$PYTHONPATH:"../../classifiers-lib/:../../classifiers-lib/p4t_native/cmake-build-release/p4t";
  '';
}
