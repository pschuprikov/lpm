import logging
from typing import Sequence

import click

import bfrt_grpc.client
from bfrt_grpc.client import _Table
from bfrt_grpc.client import KeyTuple
import bfrt_grpc.info_parse

import cls.parsers.parsing as parsing
from cls.vmrs.filter import Filter


@click.group()
def p4_runtime():
    pass


@p4_runtime.command()
def list_entries():
    client = bfrt_grpc.client.ClientInterface('localhost:50052', 0, 0)
    client.bind_pipeline_config('p4_predicat')

    bfrt_info = client.bfrt_info_get()
    ipv4_lpm_table: _Table = bfrt_info.table_get('ipv4_lpm')

    target = bfrt_grpc.client.Target()

    for data, key in ipv4_lpm_table.entry_get(target):
        print(key)

    del client


@p4_runtime.command()
def clean_entries():
    client = bfrt_grpc.client.ClientInterface('localhost:50052', 0, 0)
    client.bind_pipeline_config('p4_predicat')

    bfrt_info = client.bfrt_info_get()
    ipv4_lpm_table: _Table = bfrt_info.table_get('ipv4_lpm')

    target = bfrt_grpc.client.Target()

    ipv4_lpm_table.entry_del(target)

    del client


@p4_runtime.command()
@click.argument('classifier', type=str)
def install(classifier):
    client = bfrt_grpc.client.ClientInterface('localhost:50052', 0, 0)
    client.bind_pipeline_config('p4_predicat')

    bfrt_info = client.bfrt_info_get()

    ipv4_lpm_table: _Table = bfrt_info.table_get('ipv4_lpm')
    ipv4_lpm_table_info: bfrt_grpc.info_parse._TableInfo = ipv4_lpm_table.info

    print_table_info(ipv4_lpm_table_info)

    target = bfrt_grpc.client.Target()

    parser = parsing.text(104)

    with open(classifier, 'r') as classifier_file:
        for filter_str, action_str in (line.split('\t') for line in classifier_file):
            action = int(action_str)
            filter: Filter = parser.parse_line(filter_str, 0)[0]

            if _get_total_key_size(ipv4_lpm_table_info) != len(filter):
                raise RuntimeError(f'key length mismatch: {_get_total_key_size(ipv4_lpm_table_info)} expected got {len(filter)}')

            logging.info(f'creating key from: {filter}')

            key = create_key_from_filter(filter, ipv4_lpm_table)

            logging.info(f'created key {key}')
            data = create_data_from_action(ipv4_lpm_table, 'PredicatIngress.set_nhop', action)

            ipv4_lpm_table.entry_add(target, [key], [data])

    del client


def create_data_from_action(ipv4_lpm_table: _Table, action_name: str, action: int):
    return ipv4_lpm_table.make_data([], action_name=action_name)


def create_key_from_filter(filter, ipv4_lpm_table: _Table):
    key_fields = []
    current_bit_idx = 0
    for kn, ki in ipv4_lpm_table.info.key_dict.items():
        ksize_by, ksize_bi = ki.size
        if _is_builtin_key(kn):
            continue
        sub_filter = filter[current_bit_idx: ksize_bi]
        current_bit_idx += current_bit_idx

        if ki.match_type == 'Exact':
            key_tuple = KeyTuple(
                kn,
                value=bytearray(bool_sequence_to_bytes(sub_filter.value, ksize_by)))
        elif ki.match_type == 'Ternary':
            key_tuple = KeyTuple(
                kn,
                value=bytearray(bool_sequence_to_bytes(sub_filter.value, ksize_by)),
                mask=bytearray(bool_sequence_to_bytes(sub_filter.mask, ksize_by)))
        else:
            raise RuntimeError(f'Unknown key match type: {ki.match_type}')

        key_fields.append(key_tuple)

    return ipv4_lpm_table.make_key(key_fields)


def bool_sequence_to_bytes(s: Sequence[bool], num_bytes: int) -> bytes:
    return int(''.join('1' if x else '0' for x in s), 2).to_bytes(num_bytes, byteorder='big')


def print_table_info(table_info: bfrt_grpc.info_parse._TableInfo):
    print(f'Table "{table_info.name}"')
    print('  Keys')
    for kn, ki in table_info.key_dict.items():
        if _is_builtin_key(kn):
            continue
        ki: bfrt_grpc.info_parse._KeyInfo = ki
        print(f'     {kn}:', ki.size[1], 'bits')
    print('  Actions')
    for an, ai in table_info.action_dict.items():
        print(f'      {an}')
        for dfn, dfi in ai.data_dict.items():
            print(f'        {dfn}:', dfi.size[1], 'bits')


def _is_builtin_key(key_name: str) -> bool:
    return key_name.startswith('$')


def _get_total_key_size(table_info: bfrt_grpc.info_parse._TableInfo) -> int:
    return sum(
        ki.size[1] for kn, ki in table_info.key_dict.items()
        if not _is_builtin_key(kn)
    )


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    p4_runtime()
