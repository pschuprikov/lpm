from __future__ import annotations
import math
import os
import os.path
import abc
from itertools import groupby
from typing import List, NamedTuple, Dict, NewType, Optional
import json

import click

from cls.classifiers.meta import Meta, TableMeta, MatchType

from ioutils import P4Writer


class GroupDescription(abc.ABC):
    @property
    @abc.abstractmethod
    def bits(self) -> Optional[List[int]]:
        pass

    @property
    @abc.abstractmethod
    def match_type(self) -> MatchType:
        pass


class TableMetaGroupDescription(GroupDescription):
    def __init__(self, info: TableMeta):
        self._info = info

    @property
    def bits(self) -> Optional[List[int]]:
        return self._info.bits

    @property
    def match_type(self) -> MatchType:
        return self._info.match_type


class RestGroupDescription(GroupDescription):
    @property
    def bits(self) -> Optional[List[int]]:
        return None

    @property
    def match_type(self) -> MatchType:
        return MatchType.TERNARY


class Group(NamedTuple):
    name: str
    size: int
    info: GroupDescription

    @property
    def hit_action_name(self):
        return f'action_{self.name}_hit'

    @property
    def miss_action_name(self):
        return f'action_{self.name}_miss'

    @staticmethod
    def from_table_meta(name: str, root: str, info: TableMeta) -> Group:
        with open(os.path.join(root, info.path), 'r') as tablef:
            table_size = sum(1 for _ in tablef)
        return Group(name, table_size, TableMetaGroupDescription(info))


class P4Field(NamedTuple):
    name: str
    num_bits: int

@click.command()
@click.option('--meta', default='meta', type=str)
@click.option('--header', default='hdr', type=str)
@click.option('--fraction', type=float)
@click.option('--alpm', is_flag=True)
@click.option('--extract-keys', is_flag=True)
@click.option('--output-dir', type=str, default='test', 
              help='output directory')
@click.argument('metainfo', type=str, required=True)
def main(
    meta: str,
    header: str,
    metainfo: str,
    fraction: Optional[float],
    extract_keys: bool,
    alpm: bool,
    output_dir: str,
):
    root = os.path.dirname(os.path.abspath(metainfo))
    with open(metainfo, 'r') as infof:
        info = Meta.from_json(json.load(infof))

    groups: List[Group] = []

    for i, g in enumerate(info.groups):
        groups.append(Group.from_table_meta(f'group_{i}', root, g))

    rest: Optional[Group] = None
    if info.tcam is not None:
        rest = Group.from_table_meta(f'group_rest', root, info.tcam)

    if fraction is not None:
        groups = _ensure_fraction(fraction, groups, rest)

    base_name = 'ipv4_lpm'
    meta_name = f'{meta}.{base_name}_meta'
    header_name = f'{header}.{base_name}_header'
    combiner = TablePredicationCombiner(meta_name, base_name)
    selector: KeyProvider = _create_key_provider(header, header_name, meta_name, extract_keys)

    gen_dir = os.path.join(output_dir, 'gen')
    os.makedirs(gen_dir, exist_ok=True)

    with open(os.path.join(gen_dir, f'{base_name}_meta.p4'),
              'w') as meta_file:
        writer = P4Writer(meta_file)
        write_metadata(writer, f'{base_name}_meta', groups, combiner, selector)
        write_header(writer, f'{base_name}_header', groups, selector)

    with open(os.path.join(gen_dir, f'{base_name}_def.p4'),
              'w') as def_file:
        writer = P4Writer(def_file)
        if alpm:
            _write_alpm_impl(writer)
        for group in groups:
            write_group_actions(writer, group, combiner)
            write_group_def(writer, group, selector, alpm)
        combiner.write_actions(writer, groups)            

    with open(os.path.join(gen_dir, f'{base_name}_control.p4'),
              'w') as control_file:
        writer = P4Writer(control_file)
        for group in groups:
            selector.write_control(writer, group)
        combiner.write_control(writer, groups)


def _create_key_provider(header: str, header_name: str, meta_name: str, extract_keys: bool) -> KeyProvider:
    if not extract_keys:
        return KeySelector(
            list(reversed([
                P4Field(f'{header}.ipv4.srcAddr', 32),
                P4Field(f'{header}.ipv4.dstAddr', 32),
                P4Field(f'{header}.ipv4.protocol', 8),
                P4Field(f'{header}.tcp.srcPort', 16),
                P4Field(f'{header}.tcp.dstPort', 16),
            ])), meta_name)
    else:
        return KeyExtractor(104, header_name)


def _write_alpm_impl(writer: P4Writer):
    writer.writelines([
        'Alpm(number_partitions=1024, subtrees_per_partition= 2) alpm;']);


def _ensure_fraction(
        fraction: float,
        groups: List[Group],
        rest: Optional[Group]
        ) -> List[Group]:

    num_rules_total = sum(g.size for g in groups)
    if rest is not None:
        num_rules_total += rest.size
    num_group_rules_needed = int(math.ceil(fraction * num_rules_total))


    needed_groups: List[Group] = []
    for g in groups:
        if sum(g.size for g in needed_groups) >= num_group_rules_needed:
            break
        needed_groups.append(g)

    rest_groups = groups[len(needed_groups):]
    if rest is not None:
        rest_groups.append(rest)

    if sum(g.size for g in rest_groups) == 0:
        return needed_groups

    rest_group = Group(
            name='group_rest',
            size=sum(g.size for g in rest_groups),
            info=RestGroupDescription())

    return needed_groups + [rest_group]


FieldIdx = NewType('FieldIdx', int)


class KeyProvider(abc.ABC):
    @abc.abstractmethod
    def write_header(self, out: P4Writer, groups: List[Group]):
        pass

    @abc.abstractmethod
    def write_metadata(self, out: P4Writer, groups: List[Group]):
        pass
    
    @abc.abstractmethod
    def write_control(self, out: P4Writer, group: Group):
        pass

    @abc.abstractmethod
    def get_key(self, group: Group) -> str:
        pass


class KeySelector(KeyProvider):
    def __init__(
            self, 
            fields: List[P4Field],
            meta_name: str,
            ):
        self.fields = fields
        self.meta_name = meta_name

        self.bit_field_idx: Dict[int, FieldIdx] = {}
        self.offset: Dict[FieldIdx, int] = {}
        
        cur_bit = 0
        for i, field in enumerate(fields):
            self.offset[FieldIdx(i)] = cur_bit
            for _ in range(0, field.num_bits):
                self.bit_field_idx[cur_bit] = FieldIdx(i)
                cur_bit += 1

    def write_header(self, out: P4Writer, groups: List[Group]):
        pass

    def write_metadata(self, out: P4Writer, groups: List[Group]):
        for group in groups:
            out.writelines([
                f'bit<{self._get_group_width(group)}> {self._key_metavar(group.name)};'])

    def write_control(self, out: P4Writer, group: Group):
        subkeys = self._get_subkeys(group)
        out.writelines([
            f'{self.meta_name}.{self._key_metavar(group.name)} = ' + ' ++ '.join(subkeys) + ';'
            ])

    def _get_group_width(self, group: Group) -> int:
        if group.info.bits is not None:
            return len(group.info.bits)
        return sum(f.num_bits for f in self.fields)

    def _get_subkeys(self, group: Group) -> List[str]:
        if group.info.bits is not None:
            return self._get_subkeys_from_bits(group.info.bits)
        return [f.name for f in self.fields]

    def _get_subkeys_from_bits(self, bits: List[int]) -> List[str]:
        subkeys = []
        for fi, f_bits in groupby(bits, lambda i: self.bit_field_idx[i]):
            first_bit = None
            last_bit = None
            for bit in f_bits:
                bit -= self.offset[fi]
                if last_bit is not None and last_bit + 1 != bit:
                    subkeys.append(f'{self.fields[fi].name}[{last_bit}:{first_bit}]')
                    first_bit = None
                    last_bit = None
                if first_bit is None:
                    first_bit = bit
                last_bit = bit
            if last_bit is not None:
                subkeys.append(f'{self.fields[fi].name}[{last_bit}:{first_bit}]')
        subkeys.reverse()
        return subkeys

    def get_key(self, group: Group) -> str:
        return f'{self.meta_name}.{self._key_metavar(group.name)}'

    @staticmethod
    def _key_metavar(group_name: str):
        return f'{group_name}_key'


class KeyExtractor(KeyProvider):
    def __init__(
            self, 
            total_num_bits: int,
            header_name: str,
            ):
        self.header_name = header_name
        self.total_num_bits = total_num_bits

    def write_header(self, out: P4Writer, groups: List[Group]):
        total_width = 0
        for group in groups:
            cur_group_width = self._get_group_width(group)
            total_width += cur_group_width
            out.writelines([
                f'bit<{cur_group_width}> {self._key_metavar(group.name)};'])
        padding = total_width % 8
        if padding > 0:
            out.writelines([f'bit<{8 - padding}> padding;'])

    def write_metadata(self, out: P4Writer, groups: List[Group]):
        pass

    def write_control(self, out: P4Writer, group: Group):
        pass

    def get_key(self, group: Group) -> str:
        return f'{self.header_name}.{self._key_metavar(group.name)}'

    @staticmethod
    def _key_metavar(group_name: str):
        return f'{group_name}_key'

    def _get_group_width(self, group: Group) -> int:
        if group.info.bits is not None:
            return len(group.info.bits)
        return self.total_num_bits


class Combiner(abc.ABC):
    def __init__(self, meta_name: str, base_name: str):
        self.meta_name = meta_name
        self.base_name = base_name

    @abc.abstractmethod
    def on_hit(self, out: P4Writer, group: Group, entry_id_arg: str):
        raise NotImplementedError

    @abc.abstractmethod
    def on_miss(self, out: P4Writer, group: Group):
        raise NotImplementedError

    @abc.abstractmethod
    def write_control(self, out: P4Writer, groups: List[Group]):
        raise NotImplementedError

    @abc.abstractmethod
    def write_metadata(self, out: P4Writer, groups: List[Group]):
        raise NotImplementedError

    @abc.abstractmethod
    def write_actions(self, out: P4Writer, groups: List[Group]):
        raise NotImplementedError


class MinEntryIdCombiner(Combiner):
    def on_hit(self, out: P4Writer, group: Group, entry_id_arg: str):
        out.writelines([
            f'{self.meta_name}.{self._entry_id_metavar(group.name)} = {entry_id_arg} + 1;'
            ])

    def on_miss(self, out: P4Writer, group: Group):
        out.writelines([
            f'{self.meta_name}.{self._entry_id_metavar(group.name)} = 0;'
            ])

    def write_control(self, out: P4Writer, groups: List[Group]):
        for group in groups:
            self._write_group_control(out, group)
        self._write_combine_control(out)

    def _write_group_control(self, out: P4Writer, group: Group):
        out.writelines([f'{group.name}.apply();'])


    def _write_combine_control(self, out: P4Writer):
        out.writelines([f'{self.base_name}_combine();'])

    @staticmethod
    def _entry_id_metavar(table_name: str) -> str:
        return f'{table_name}_entry_id'

    def write_metadata(self, out: P4Writer, groups: List[Group]):
        for group in groups:
            out.writelines([f'bit<32> {self._entry_id_metavar(group.name)};'])
        out.writelines([f'  bit<32> entry_id_min;'])

    def write_actions(self, out: P4Writer, groups: List[Group]):
        result_var = f'{self.meta_name}.entry_id_min'
        with out.block(f'action {self.base_name}_combine()'):
            out.writelines([f'{result_var} = 0;']) 
            for group in groups:
                out.writelines([
                    f'{result_var} = {result_var} < {self.meta_name}.{self._entry_id_metavar(group.name)} ? {self.meta_name}.{self._entry_id_metavar(group.name)} : {result_var};'
                    ])


class TablePredicationCombiner(Combiner):
    def on_hit(self, out: P4Writer, _: Group, entry_id_arg: str):
        out.writelines([f'{self.meta_name}.entry_id = {entry_id_arg} + 1;'])

    def on_miss(self, out: P4Writer, _: Group):
        out.writelines([f'{self.meta_name}.entry_id = 0;'])

    def write_control(self, out: P4Writer, groups: List[Group]):
        self._write_control_table_predication_rec(out, groups)

    def write_actions(self, _out: P4Writer, _group: List[Group]):      
        pass

    def _write_control_table_predication_rec(
            self, out: P4Writer, groups: List[Group]):
        group, *rest = groups

        if len(rest) == 0:
            out.writelines([f'{group.name}.apply();'])
        else:
            with out.block(f'if ({group.name}.apply().miss)'):
                self._write_control_table_predication_rec(out, rest)

    def write_metadata(self, out: P4Writer, _: List[Group]):
        out.writelines([f'bit<32> entry_id;'])

def write_group_def(out: P4Writer, group: Group, selector: KeyProvider, alpm: bool):
    with out.block(f'table {group.name}'):
        with out.block('key = '):
            out.writelines([
                f'{selector.get_key(group)} : {group.info.match_type.name.lower()};',
                ])
        with out.block('actions = '):
            out.writelines([
                f'{group.hit_action_name};',
                f'{group.miss_action_name};',
                ])
        out.writelines([f'size = {group.size};'])
        if alpm and group.info.match_type == MatchType.LPM:
            out.writelines(['implementation = alpm;'])

def write_group_actions(out: P4Writer, group: Group, combiner: Combiner):
    with out.block(f'action {group.hit_action_name}(bit<32> entry_id)'):
        combiner.on_hit(out, group, 'entry_id')
    with out.block(f'action {group.miss_action_name}()'):
        combiner.on_miss(out, group)


def write_metadata(
        out: P4Writer, 
        meta_name: str, 
        groups: List[Group],
        combiner: Combiner,
        selector: KeyProvider):
    with out.block(f'struct {meta_name}'):
        combiner.write_metadata(out, groups)
        selector.write_metadata(out, groups)

def write_header(
        out: P4Writer, 
        header_name: str, 
        groups: List[Group],
        selector: KeyProvider):
    with out.block(f'header {header_name}_t'):
        selector.write_header(out, groups)


if __name__ == '__main__':
    main()
