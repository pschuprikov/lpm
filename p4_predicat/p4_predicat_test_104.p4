/* Copyright 2013-present Barefoot Networks, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include<core.p4>
#if __TARGET_TOFINO__ == 2
#include <t2na.p4>
#else
#include <tna.p4>
#endif

#include "bf/util.p4"

action mark_to_drop(inout ingress_intrinsic_metadata_for_deparser_t meta) {
    meta.drop_ctl = 0x1;
}

const bit<16> TYPE_IPV4 = 0x800;
const bit<8>  TYPE_TCP  = 6;

typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header ipv4_t {
    bit<4> version;
    bit<4> ihl;
    bit<8> diffserv;
    bit<16> totalLen;
    bit<16> identification;
    bit<3> flags;
    bit<13> fragOffset;
    bit<8> ttl;
    bit<8> protocol;
    bit<16> hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

header tcp_t {
    bit<16> srcPort;
    bit<16> dstPort;
    bit<32> seqNo;
    bit<32> ackNo;
    bit<4>  dataOffset;
    bit<4>  res;
    bit<1>  cwr;
    bit<1>  ece;
    bit<1>  urg;
    bit<1>  ack;
    bit<1>  psh;
    bit<1>  rst;
    bit<1>  syn;
    bit<1>  fin;
    bit<16> window;
    bit<16> checksum;
    bit<16> urgentPtr;
}

struct headers {
    ethernet_t ethernet;
    ipv4_t ipv4;
    tcp_t tcp;
}

struct metadata {
}

parser PredicatIngressParser(
        packet_in pkt,
        out headers hdr,
        out metadata meta,
        out ingress_intrinsic_metadata_t ig_intr_md) {

    TofinoIngressParser() tofino_parser;

    state start {
        tofino_parser.apply(pkt, ig_intr_md);
        transition parse_ethernet;
    }

    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4 : parse_ipv4;
            default: reject;
        }
    }

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        transition select(hdr.ipv4.protocol) {
            TYPE_TCP : parse_tcp;
            default: reject;
        }
    }

    state parse_tcp {
        pkt.extract(hdr.tcp);
        transition parse_keys;
    }

    state parse_keys {
        transition accept;
    }
}

control PredicatIngress(inout headers hdr,
                        inout metadata meta,
                        in ingress_intrinsic_metadata_t ig_intr_md,
                        in ingress_intrinsic_metadata_from_parser_t ig_intr_prsr_md,
                        inout ingress_intrinsic_metadata_for_deparser_t ig_intr_dprsr_md,
                        inout ingress_intrinsic_metadata_for_tm_t ig_intr_tm_md) {
    action drop() {
        ig_intr_dprsr_md.drop_ctl = 0x1; // Drop packet.
    }

    action set_tos(bit<8> tos) {
        ig_intr_tm_md.ucast_egress_port = 0;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
        hdr.ipv4.diffserv = tos;
    }

    Alpm(number_partitions=PREDICAT_NUMBER_PARTITIONS, subtrees_per_partition=1) alpm;

    bit<104> super_key;

    table forward {
        key = {
            super_key: lpm;
        }
        actions = {
            set_tos;
            drop;
        }

        const default_action = drop;
        implementation = alpm;
        size=PREDICAT_SIZE;
    }

    apply {
        if(hdr.ipv4.isValid()) {
            super_key = hdr.ipv4.srcAddr ++ hdr.ipv4.dstAddr ++ hdr.ipv4.protocol ++ hdr.tcp.srcPort ++ hdr.tcp.dstPort;
            forward.apply();
        }
    }
}

control PredicatIngressDeparser(
        packet_out pkt,
        inout headers hdr,
        in metadata ig_md,
        in ingress_intrinsic_metadata_for_deparser_t ig_intr_dprsr_md) {
    apply {
        pkt.emit(hdr);
    }
}

Pipeline(PredicatIngressParser(),
         PredicatIngress(),
         PredicatIngressDeparser(),
         EmptyEgressParser(),
         EmptyEgress(),
         EmptyEgressDeparser()) pipe;

Switch (pipe) main;
