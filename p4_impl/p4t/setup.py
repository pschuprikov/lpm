from setuptools import setup, find_packages

setup(
    name='p4t',
    version='0.0.1',
    description='P4 transformation infrastructure',
    license='Apache-2.0',
    packages=find_packages(exclude=['test*']),
    install_requires=['p4-hlir', 'p4c-bm', 'bitstring', 'cls'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
