with import <nixpkgs> {};
let python = pkgs.python.withPackages (ps: [ p4c_bm behavioral_model ps.bitstring ps.ipython boost ps.pylint ps.readline ps.flake8 ps.pytest ps.pytestrunner ]);
in pkgs.stdenv.mkDerivation {
  inherit python;
  name = "p4t-env";
  src = ./.;
  nativeBuildInputs = with pkgs; [ boost gperftools python ];
  PYTHONPATH = "../p4t_native/build:./";
}
