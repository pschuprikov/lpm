{ nixpkgs ? import <nixpkgs> {}}:
nixpkgs.lib.makeScope nixpkgs.newScope (self: rec {
  tenjin = nixpkgs.pythonPackages.buildPythonPackage rec {
    name = "tenjin-${version}";
    version = "1.1.1";

    src = nixpkgs.fetchurl {
      url = "http://pypi.python.org/packages/source/T/Tenjin/Tenjin-${version}.tar.gz";
      sha256 = "15s681770h7m9x29kvzrqwv20ncg3da3s9v225gmzz60wbrl9q55";
    };
  };

  nnpy = nixpkgs.pythonPackages.buildPythonPackage rec {
    name = "nnpy-${version}";
    version = "1.3";

    propagatedBuildInputs = [ nixpkgs.pythonPackages.cffi nixpkgs.nanomsg ];

    preBuild = ''
      cat <<EOF > site.cfg
      [DEFAULT]
      include_dirs=${nixpkgs.nanomsg}/include/nanomsg
      host_library=${nixpkgs.nanomsg}/lib/libnanomsg.so
      EOF
    '';

    src = nixpkgs.fetchFromGitHub {
      owner = "nanomsg";
      repo = "nnpy";
      rev = "01ba381d1de7213ea7db14302cc7cb7244172854";
      sha256 = "0463pa0pfz6hpyz28bi4agb0h09m8l4z9d912is7nzaix492cn1v";
    };
  };

  p4c_bm = self.callPackage ./p4c-bm { };

  p4_hlir_1_0 = self.callPackage ./p4-hlir/1.0.nix { };
  p4_hlir_1_1 = self.callPackage ./p4-hlir/1.1.nix { };

  behavioral_model = self.callPackage ./behavioral-model { enableDebugger = true; };
  mininet = self.callPackage ./mininet { };

  p4t = self.callPackage ./p4t {};
})
