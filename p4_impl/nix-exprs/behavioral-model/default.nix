{ stdenv, autoreconfHook, fetchFromGitHub, pythonPackages,
  thrift, boost, judy, gmp, nanomsg, libpcap, openssl, nnpy,
  enableTargets ? false,
  enableDebugger ? true }:

stdenv.mkDerivation rec {
  name = "p4-behavioral-model-${version}";
  version = "1.3.0";

  pythonPath = [ pythonPackages.thrift nnpy];

  src = fetchFromGitHub {
    owner = "p4lang";
    repo = "behavioral-model";
    rev = "e182c0f1876541221fdeb27c596cc95d054b391f";
    sha256 = "0nliqm1x5pcp20myx5wal78zj6z5lsqbj7va38hkk7aq3bwr0krw";
  };

  nativeBuildInputs = [ autoreconfHook ];
  buildInputs = [ pythonPackages.python boost thrift judy gmp nanomsg libpcap openssl pythonPackages.wrapPython ];

  configureFlags = 
    stdenv.lib.optional (!enableTargets) "--without-targets" ++
    stdenv.lib.optional enableDebugger "--enable-debugger";

  postPatch = ''
    patchShebangs tools/get_version.sh
    echo ${builtins.substring 0 8 src.rev} > VERSION-build
  '';

  postInstall = ''
    wrapPythonPrograms

    rm $out/lib/*.la
  '';
}
