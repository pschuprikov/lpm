{ pythonPackages, fetchFromGitHub, fetchurl, tenjin, p4_hlir_1_1, p4_hlir_1_0 }:
pythonPackages.buildPythonPackage rec {
  name = "p4c-bm-${version}";
  version = "1.3.0";

  propagatedBuildInputs = with pythonPackages; [ tenjin p4_hlir_1_1 p4_hlir_1_0 ply ];

  doCheck = false;

  src = fetchFromGitHub {
    owner = "p4lang";
    repo = "p4c-bm";
    rev = "6482e34e524e208bd4ccd9af832bf9032c9ad9f8";
    sha256 = "0chrvl3i9a6qrjyj2zskwb1rl5qr4qa0prgw5j09aw7x0v0k4f5w";
  };
}

  
