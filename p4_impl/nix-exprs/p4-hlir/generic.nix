{ pythonPackages, fetchFromGitHub, gcc, src, version, ... }:
pythonPackages.buildPythonPackage rec {
  inherit src version;

  name = "p4-hlir-${version}";

  propagatedBuildInputs = with pythonPackages; [ gcc ply ];
}
