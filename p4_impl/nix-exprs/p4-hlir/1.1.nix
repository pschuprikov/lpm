{ callPackage, fetchFromGitHub, ... } @ args:
callPackage ./generic.nix (args // {
  version = "1.1";

  src = fetchFromGitHub {
    owner = "p4lang";
    repo = "p4-hlir";
    rev = "fdee55e2567fe65463f328d70558b5079894b420";
    sha256 = "0mixc6xzq3x99lq32h3nd2snhy8nsqzc2a9i2blhx9plmqxkzwpj";
  };
})
