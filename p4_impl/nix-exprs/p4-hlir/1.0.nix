{ callPackage, fetchFromGitHub, ... } @ args:
callPackage ./generic.nix (args // {
  version = "1.0";

  src = fetchFromGitHub {
    owner = "p4lang";
    repo = "p4-hlir";
    rev = "4ad9d236aba95800c0ea08c30b36ff831e932dd2";
    sha256 = "1cxwhklzhmmph64q7sqscza6pmq9i6gif3pjkgc2crxli8wwiqlq";
  };
})
