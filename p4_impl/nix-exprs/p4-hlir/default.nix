{ pythonPackages, fetchFromGitHub, gcc }:
pythonPackages.buildPythonPackage rec {
  name = "p4-hlir-${version}";
  version = "0.9.39";

  buildInputs = with pythonPackages; [ ply ];
  propagatedBuildInputs = [ gcc ];

  src = fetchFromGitHub {
    owner = "p4lang";
    repo = "p4-hlir";
    rev = "4ad9d236aba95800c0ea08c30b36ff831e932dd2";
    sha256 = "1cxwhklzhmmph64q7sqscza6pmq9i6gif3pjkgc2crxli8wwiqlq";
  };
}

  
