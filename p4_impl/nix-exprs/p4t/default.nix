{ pythonPackages, p4c_bm, behavioral_model, boost }: 

pythonPackages.buildPythonPackage {
  name = "p4t";
  version = "0.0.1";

  src = ../../p4t;

  buildInputs = with pythonPackages; [ 
    pytestrunner bitstring p4c_bm pytest behavioral_model thrift 
    pythonPackages.python boost];

  doCheck = false;
}
