{ stdenv, fetchFromGitHub, pythonPackages, help2man, telnet }:
pythonPackages.buildPythonPackage rec {
  name = "mininet-${version}";
  version = "2.2.1";
  
  src = fetchFromGitHub {
    owner = "mininet";
    repo = "mininet";
    rev = "8b2881b0c2f40df5936410b2222b202f68c50a31";
    sha256 = "1nq55vzv021xnmz1mdh4xlba50mlbpwllwip4d2pq6prfqngsdab";
  };

  propagatedBuildInputs = [ telnet ];

  buildInputs = [ help2man ];

  patchPhase = ''
    substituteInPlace Makefile --replace /usr $out
    sed -i Makefile -e '/python setup\.py install/d'
  '';

  postBuild = ''
    make mnexec
  '';

  preInstall = ''
    mkdir -p $out/share/man/man1
  '';

  postInstall = ''
    make install
  '';
}
