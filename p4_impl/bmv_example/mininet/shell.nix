with import <nixpkgs> {};
let python = pkgs.python.withPackages (ps: [ mininet ps.click ] );
in pkgs.stdenv.mkDerivation {
  inherit python;
  name = "mininet-env";
  buildInputs = [ python ];
}
