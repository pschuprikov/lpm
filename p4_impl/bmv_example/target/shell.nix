{ nixpkgs ? import <nixpkgs> {} }:
nixpkgs.stdenv.mkDerivation {
  name = "simpler_router";
  src = ./.;
  buildInputs = with nixpkgs; [ cmake pkgconfig behavioral_model boost gmp judy thrift ];
}
