{ nixpkgs ? import <nixpkgs> {} }:
nixpkgs.stdenv.mkDerivation {
  name = "p4-build-env";
  src = ./.;
  buildInputs = [ nixpkgs.p4c_bm ];
}
