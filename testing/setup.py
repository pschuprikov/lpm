from distutils.core import setup

setup(
    name="congestion-runner",
    version="0.1dev",
    packages=['predicat_runner'],
    install_requires=[
        'click', 'classifiers-lib'
    ],
    entry_points='''
        [console_scripts]
        run=predicat_runner.checker:greet
    '''
)
