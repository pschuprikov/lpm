import functools
import hashlib
import logging
import pickle
import asyncio
from asyncio import Lock, isfuture
from shelve import Shelf
from typing import Any, Awaitable, Callable, Optional, TypeVar, Union
from typing_extensions import ParamSpec


class CacheLoadError(BaseException):
    pass


class Cache:
    logger = logging.getLogger('caching')

    def __init__(self, shelf: Shelf[tuple]):
        self.shelf = shelf
        self.lock = Lock()

    async def load(self, key) -> Any:
        pickled_key_hash = self._get_pickled_key_hash(key)

        async with self.lock:
            if pickled_key_hash not in self.shelf:
                self.logger.info(
                    f'no bucket for key {key} (hash is {pickled_key_hash})')
                raise CacheLoadError

            hash_bucket = self._get_bucket_checked(key, pickled_key_hash)

            for o_key, o_value in hash_bucket:
                if self._compare_keys(o_key, key):
                    self.logger.info(
                        f'found value {o_value} for key {key} (hash is {pickled_key_hash})'
                    )
                    return o_value

            self.logger.info(
                f'bucket has no key {key} (hash is {pickled_key_hash})')
            raise CacheLoadError

    @staticmethod
    def _compare_keys(old_key, lookup_key) -> bool:
        try:
            return old_key == lookup_key
        except AttributeError as e:
            logging.warning(f'failed key comparison against {lookup_key}: {e}')

    def _get_bucket_checked(self, key, pickled_key_hash: str) -> tuple:
        try:
            return self.shelf[pickled_key_hash]
        except pickle.UnpicklingError as e:
            self.logger.warning(
                f'could not unpickle for key {key} (hash is {pickled_key_hash})'
            )
            self.logger.info(f'failure reason: {e}')
            # we don't have any mechanism to selectively unpickle
            del self.shelf[pickled_key_hash]
            return tuple()

    async def store(self, key, value):
        pickled_key_hash = self._get_pickled_key_hash(key)

        self.logger.info(
            f'storing value {value} under key {key} (hash is {pickled_key_hash})'
        )

        async with self.lock:
            if pickled_key_hash not in self.shelf:
                self.shelf[pickled_key_hash] = tuple()

            old_hash_bucket = self._get_bucket_checked(key, pickled_key_hash)

            self.shelf[pickled_key_hash] = (key, value), *old_hash_bucket

    @staticmethod
    def _get_pickled_key_hash(key) -> str:
        try:
            pickled_key = pickle.dumps(key)
        except TypeError:
            logging.error(f"Cannot pickle key: {key}")
            raise

        pickled_key_hash = hashlib.sha256(pickled_key).hexdigest()
        return pickled_key_hash


T = TypeVar('T')
P = ParamSpec('P')


def cached(
        cache_provider: Callable[[],Optional[Cache]],
        version: Optional[int] = None,
        force: Union[bool, Callable[...,bool]] = False
) -> Callable[[Callable[P, Awaitable[T]]], Callable[P, Awaitable[T]]]:

    def decorator(func: Callable[P, Awaitable[T]]):
        @functools.wraps(func)
        async def decorated_func(*args: P.args, **kwargs: P.kwargs) -> T:
            cache = cache_provider()
            if cache is None:
                return await func(*args, **kwargs)

            key = await _key_from_args(func, version, *args, **kwargs)

            awaited_args = await _args_to_result(*args)
            awaited_kwargs = await _kwargs_to_result(**kwargs)
            if isinstance(force, bool) and not force or not force(*awaited_args, **awaited_kwargs):
                try:
                    cached_result = await cache.load(key)
                    Cache.logger.info(f'lookup result for {key} is {cached_result}')
                    return cached_result
                except CacheLoadError:
                    pass
            else:
                Cache.logger.warning(f'forcing {key}')

            computed_result = await func(*args, **kwargs)
            await cache.store(key, computed_result)
            return computed_result

        async def get_cached(*args: P.args, **kwargs: P.kwargs) -> T:
            cache = cache_provider()
            if cache is None:
                raise AssertionError()

            key = await _key_from_args(func, version, *args, **kwargs)
            return await cache.load(key)

        decorated_func.get_cached = get_cached

        return decorated_func


    async def _key_from_args(func, version, *args, **kwargs):
        return (func.__name__, version,
                await _args_to_result(*args),
                tuple(sorted((await _kwargs_to_result(**kwargs)).items())),
                )

    async def _args_to_result(*args):
        return tuple(await asyncio.gather(*(
            _arg_to_result(arg)
            for arg in args)))

    async def _kwargs_to_result(**kwargs) -> dict:
        return {n: await _arg_to_result(a)
                for n, a in kwargs.items()}

    async def _arg_to_result(x):
        if isfuture(x):
            return await x
        return x

    return decorator
