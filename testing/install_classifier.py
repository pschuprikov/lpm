import sys
import bfrt_grpc.client as gc


def main(cls_fname):
    target = gc.Target(device_id=0, pipe_id=0xffff)
    for _ in range(10):
        try:
            interface = gc.ClientInterface('localhost:50052', client_id=0, device_id=0, num_tries=1)
        except RuntimeError:
            continue
        break
    else:
        exit(2)

    bfrt_info = interface.bfrt_info_get()
    interface.bind_pipeline_config(bfrt_info.p4_name_get())
    tbl = bfrt_info.table_get('forward')
    with open(cls_fname) as cls_file:
        all_fs = set()
        for row in cls_file:
            f, action = row.split('\t')
            f = f + '*' * (104 - len(f))
            action = int(action)
            data = tbl.make_data([gc.DataTuple('tos', action % 256)],
                                 'PredicatIngress.set_tos')
            key = tbl.make_key(
                [gc.KeyTuple('super_key',
                             value=filter_to_bytearray(f),
                             prefix_len=filter_prefix_len(f))])
            if bytes(filter_to_bytearray(f)) in all_fs:
                print(f'dupclicate entry: {f}')
                continue
            all_fs.add(bytes(filter_to_bytearray(f)))
            try:
                tbl.entry_add(target, [key], [data])
            except gc.BfruntimeReadWriteRpcException as e:
                if 'ALREADY_EXISTS' in str(e):
                    print(f'Uncaucht already exists: {f}')
                exit(1)


def filter_prefix_len(f: str) -> int:
    return sum(1 for b in f if b != '*')


def filter_to_bytearray(f: str) -> bytearray:
    num_bytes = (len(f) + 7) // 8
    # num_extra = num_bytes * 8 - len(f)
    bits = ['1' if b == '1' else '0' for b in f]
    n = int(''.join(bits), 2)
    return bytearray(n.to_bytes(num_bytes, 'big'))


if __name__ == '__main__':
    main(*sys.argv[1:])
