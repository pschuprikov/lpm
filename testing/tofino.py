import signal
import re
import os.path as path
from contextlib import asynccontextmanager
import asyncio
import logging
from typing import Optional, NamedTuple, IO, Union, Callable
import subprocess

import lib
from lib import tofino_model_exe, switchd_exe
from lib import InstalledP4Program


@asynccontextmanager
async def tofino_model(program: InstalledP4Program, run_dir: str):
    with open(path.join(run_dir, 'tofino_model_stdout'), 'w') as stdout_f:
        with open(path.join(run_dir, 'tofino_model_stderr'), 'w') as stderr_f:
            async with run_process('tofino_model', 'sudo', tofino_model_exe(),
                                   '--p4-target-config',
                                   program.target_config, '--chip-type',
                                   str(program.arch.chip_type),
                                   '--install-dir', program.install_dir,
                                   '--log-dir', run_dir, 
                                   stdout=stdout_f, stderr=stderr_f) as proc:
                yield proc


@asynccontextmanager
async def switchd(build: InstalledP4Program, run_dir: str, background=True):
    lib_path = path.join(sde_install_dir(), 'lib')
    with open(path.join(run_dir, 'switchd_stdout'), 'w') as stdout_f:
        with open(path.join(run_dir, 'switchd_stderr'), 'w') as stderr_f:
            async with run_process('switchd', 'sudo', 'env',
                                   f'LD_LIBRARY_PATH={lib_path}',
                                   switchd_exe(), '--conf-file',
                                   build.target_config,
                                   '--install-dir', build.install_dir,
                                   '--background' if background else '',
                                   expected_returncode=0,
                                   stdout=stdout_f, stderr=stderr_f) as proc:
                yield proc


def run_bf_python(script_path: Optional[str] = None):
    args = lib.create_nix_shell_args(
        sde_flake_url(),
        'python' + (' ' + script_path if script_path else ''),
        env={'PYTHONPATH': bf_grpc_extra_pythonpath()}
    )
    subprocess.check_call(args)


def bf_grpc_extra_pythonpath():
    sde_install = sde_install_dir()
    return ':'.join([
        path.join(sde_install, 'lib', 'python3.8', 'site-packages', 'tofino',
                  'bfrt_grpc'),
        path.join(sde_install, 'lib', 'python3.8', 'site-packages'),
        path.join(sde_install, 'lib', 'python3.8', 'site-packages', 'tofino'),
    ])


@asynccontextmanager
async def run_process(name, *args: str, 
                      stdout: Union[IO,int,None] = None,
                      stderr: Union[IO,int,None] = None,
                      expected_returncode: Optional[int] = None):
    if expected_returncode is None:
        expected_returncode = -signal.SIGTERM

    logging.info(f'running:\n' + ' '.join(args))
    proc = await asyncio.create_subprocess_exec(*args, stdout=stdout, stderr=stderr)
    try:
        yield proc
    finally:
        logging.info(f'stopping {name} process with PID {proc.pid}')
        subprocess.check_call(['kill', str(proc.pid)], start_new_session=True)
        returncode = await proc.wait()
        if returncode != expected_returncode:
            raise AssertionError(f'Unexpected return code {returncode}')

def read_table(res: str, get_section: Callable[[str], Optional[str]]) -> dict:
    section = None
    in_header = False
    current = None
    result = {}
    for line in res.split('\n'):
        line = line.strip()

        mb_section = get_section(line)
        if mb_section is not None:
            section = mb_section
        elif is_table_ascii_line(line):
            in_header = not in_header
        elif is_table_line(line):
            values = [v.strip() for v in line.split('|')]
            if in_header:
                current = {h: [] for h in values}
            elif current is not None:
                for h, v in zip(current, values):
                    current[h].append(v)
        elif section is not None and current is not None:
            result[section] = current
            section = None
            current = None
        else:
            current = None
    return result


def is_table_ascii_line(line) -> bool:
    return re.match(r'^-+(\|-+)*$', line) is not None


def is_table_line(line) -> bool:
    return re.match(r'^[^|]+(\|[^|]+)+$', line) is not None


async def get_table_data(reader, writer) -> dict:
    res = await _cli_run('tbl -d0', reader, writer)

    def get_section(line):
        if line.endswith(':'):
            return line.removesuffix(':')

    return read_table(res, get_section)


async def count_entries(tbl_id, reader, writer) -> int:
    res = await _cli_run('entry_count -d 0 -h' + tbl_id, reader, writer)
    for line in res.split('\n'):
        line = line.strip()
        if 'entries occupied' in line:
            return int(line.split()[2])


class TableSizeResult(NamedTuple):
    pre_classifier: int
    atcam: int


async def read_complete_atcam_tbl_info(tbl_id, reader, writer) -> int:
    await _cli_run('tcam_tbl', reader, writer)
    res = await _cli_run(f'tbl_info -d0 -h{tbl_id}', reader, writer)
    def get_section(line):
        if line.startswith('Partition '):
            return line.removeprefix('Partition ')
    res = read_table(res, get_section)
    await _cli_run('..', reader, writer)
    for k in res:
        print(res[k].keys())
    print(sum(len(res[k]['Index']) for k in res))
    return len(res)


async def get_table_size() -> TableSizeResult:
    reader, writer = await asyncio.open_connection('localhost', 9999, limit=2**20)
    (await reader.readuntil(b'>')).decode('ascii')

    await _cli_run('ucli', reader, writer)
    await _cli_run('pipe_mgr', reader, writer)
    table_data = await get_table_data(reader, writer)
    mats = table_data['Match-Action Tables']
    n_mats = len(mats['Name'])

    i_pre, = [i for i in range(n_mats) 
              if mats['Name'][i] == 'PredicatIngress.forward_pre_classifier']
    i_atcam, = [i for i in range(n_mats) 
                if mats['Name'][i] == 'PredicatIngress.forward'
                and mats['Type'][i] == 'atcam']

    ne_pre = await count_entries(mats['Handle'][i_pre], reader, writer)
    ne_atcam = await count_entries(mats['Handle'][i_atcam], reader, writer)

    await _cli_run('..', reader, writer)
    await _cli_run('..', reader, writer)

    writer.close()
    await writer.wait_closed()
    return TableSizeResult(ne_pre, ne_atcam)


async def _cli_run(cmd: str, reader, writer) -> str:
    writer.write(cmd.encode('ascii'))
    writer.write(b'\n')
    result = (await reader.readuntil(b'>')).decode('ascii')
    logging.debug(result)
    return result


def sde_install_dir() -> str:
    return path.dirname(path.dirname(switchd_exe()))


def sde_dir() -> str:
    return path.dirname(sde_install_dir())


def sde_flake_url() -> str:
    return f'git+file://{path.abspath(sde_dir())}'

