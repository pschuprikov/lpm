# coding: utf-8
import shelve
import run_caching
import run
import asyncio
import lib
shelf = shelve.open('.shelf')
run.Global.cache = run_caching.Cache(shelf)
asyncio.run(run.run_p4_test.get_cached())
