from typing import NamedTuple, Optional

import cls.optimizations.oi_lpm as opt


class OIParams(NamedTuple):
    algo: opt.OIAlgorithm
    cutoff: Optional[int]
    bit_width: Optional[int]
    only_exact: bool
    use_actions: bool
    bit_range: Optional[opt.BitRange]

    @property
    def kind(self) -> str:
        kind = 'oi'

        if self.only_exact:
            kind += '_exact'

        if self.use_actions:
            kind += '_act'

        if self.bit_range is not None:
            kind += (f'_bound_{self.bit_range.start_bit}_'
                     f'{self.bit_range.end_bit}')

        return kind


class LPMParams(NamedTuple):
    max_groups: Optional[int]
    max_candidate_groups: int
    max_expanded_bits: Optional[int]
    oi_lpm_algo: Optional[opt.LPMOIAlgorithm]
    oi_lpm_greedy_strategy: Optional[opt.LPMOIAlgorithm]
    oi_lpm_reduce_width_after: bool


class OILPMJointParams(NamedTuple):
    oi: OIParams
    lpm: LPMParams

    @property
    def kind(self) -> str:
        kind = 'oi_lpm_joint'

        if self.lpm.max_expanded_bits is not None:
            kind += "_exp"

        if self.lpm.max_groups is not None:
            kind += "_bounded"

        if self.oi.use_actions:
            kind += "_act"

        return kind


class OIAndLPMParams(NamedTuple):
    oi: OIParams
    lpm: LPMParams

    @property
    def kind(self):
        kind = 'lpm' if self.lpm.max_groups is None else 'lpm_bounded'
        if self.lpm.oi_lpm_greedy_strategy is not None:
            kind += '_' + self.lpm.oi_lpm_greedy_strategy.name.lower()
        if self.lpm.oi_lpm_algo is opt.LPMOIAlgorithm.INCREMENTAL:
            kind += '_inc'
        if self.oi.use_actions:
            kind += '_act'
        if self.lpm.oi_lpm_reduce_width_after:
            kind += '_postoi'
        return kind
