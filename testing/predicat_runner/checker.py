import os.path
import logging
import random
import sys
from collections import Counter
from enum import Enum
from itertools import islice
from typing import NamedTuple, Optional

import click

import cls.optimizations.native_utils as opt_native
import cls.optimizations.oi_lpm as opt
import cls.parsers.parsing as parsing
from cls.classifiers.simple import BasicClassifier
from cls.vmrs.abstract import AbstractVMREntry
from cls.vmrs.simple import FPCAction

from predicat_runner.common import OIParams, LPMParams, OIAndLPMParams,\
        OILPMJointParams
import predicat_runner.io as io


class ClassifierFormat(Enum):
    ORIGINAL = (parsing.classbench_original,)
    EXPANDED = (parsing.classbench_expanded,)
    IPS_UNIQUE = (parsing.classbench_ips_unique,)


class GlobalParams(NamedTuple):
    max_entries: Optional[int] = None
    output_file: str = 'data.tsv'
    classifier_format: ClassifierFormat = ClassifierFormat.EXPANDED
    result_dir: str = 'results'


class IncrementalParams(NamedTuple):
    min_traditional: int
    add_fraction_traditional: float
    output_file: str
    traditional_offline: Optional[int]


PARAMS = GlobalParams()
OI_PARAMS = OIParams(
    algo=opt.OIAlgorithm.ICNP_BLOCKERS,
    cutoff=20,
    bit_width=None,
    use_actions=False,
    only_exact=False,
    bit_range=None,
)
LPM_PARAMS = LPMParams(
    max_groups=None,
    max_candidate_groups=OI_PARAMS.cutoff,
    max_expanded_bits=None,
    oi_lpm_algo=None,
    oi_lpm_greedy_strategy=None,
    oi_lpm_reduce_width_after=False,
)
INCREMENTAL_PARAMS = IncrementalParams(
    min_traditional=1000,
    add_fraction_traditional=0.1,
    output_file='inc_data.tsv',
    traditional_offline=None)


def add_row(kind, filename, num_entries, oi_algorithm,
            bit_width, max_groups, num_groups, num_entries_traditional, groups,
            max_expanded_bits, expanded_groups):
    with open(PARAMS.output_file, 'a') as output_file:
        print(
            kind, filename, PARAMS.max_entries, num_entries,
            oi_algorithm.name.lower() if oi_algorithm is not None else 'NA',
            bit_width, max_groups, num_groups,
            num_entries_traditional, groups,
            max_expanded_bits, expanded_groups,
            sep='\t', file=output_file
        )


def add_inc_row(filename, num_entries, bit_width, max_groups, add_fraction,
                add_rules, num_rebuilds, total_traditional, total_lpm):
    with open(INCREMENTAL_PARAMS.output_file, 'a') as f:
        print(filename, num_entries, bit_width, max_groups, add_fraction,
              add_rules, num_rebuilds, total_traditional, total_lpm,
              sep='\t', file=f)


def read_classifier(filename, classifier_format: Optional[ClassifierFormat] = None) -> BasicClassifier:
    if classifier_format is None:
        parsing_classifier_format = PARAMS.classifier_format.value[0]
    else:
        parsing_classifier_format = classifier_format.value[0]

    with open(filename, 'r') as input_file:
        classifier = parsing.read_classifier(
            os.path.basename(filename),
            parsing_classifier_format,
            islice(input_file, 0, PARAMS.max_entries)
        )
    return classifier


@click.group()
@click.option('--max-entries', default=PARAMS.max_entries, type=int,
              help='Maximal number of entries to take from the input')
@click.option('--output_file', default=PARAMS.output_file,
              help='File to store')
@click.option('--result-dir', default=PARAMS.result_dir, type=str,
              help='Directory, where to save the optimization results')
@click.option('--num-threads', default=None, type=int,
              help='Number of threads to use')
@click.option('--oi-cutoff', default=OI_PARAMS.cutoff, type=int,
              show_default=True,
              help='Maximal allowed number of groups in any OI invocation (if unbounded set to 0)')
@click.option('--oi-algo', default=OI_PARAMS.algo.name.lower(),
              show_default=True,
              type=click.Choice(x.name.lower() for x in opt.OIAlgorithm),
              help='OI algorithm to use')
@click.option('--oi-use-actions', is_flag=True, help='use Action OI')
@click.option('--oi-bit-width', default=OI_PARAMS.bit_width, type=int,
              help='Required OI bit width')
@click.option('--oi-only-exact', is_flag=True,
              help='Use only exact bits in OI?')
@click.option('--lpm-max-groups',
              default=LPM_PARAMS.max_groups if LPM_PARAMS.max_groups is not None else 0,
              show_default=True, type=int,
              help='Maximal allowed number of groups, 0 if unbounded')
@click.option('--lpm-max-expanded-bits',
              type=int, help='Maximal number of entries')
@click.option('--lpm-max-candidate-groups', show_default=True,
              default=LPM_PARAMS.max_candidate_groups, type=int,
              help='Maximal number of candidate groups to select from, 0 if unbounded')
@click.option('--inc-add-fraction',
              default=INCREMENTAL_PARAMS.add_fraction_traditional, type=float,
              help='Added traditional memory fraction')
@click.option('--inc-traditional-offline',
              default=INCREMENTAL_PARAMS.traditional_offline,
              type=int, help='Amount of traditional memory used in offline')
@click.option('--inc-output-file', default=INCREMENTAL_PARAMS.output_file,
              help='File to store incremental updates')
@click.option('--inc-min-traditional',
              default=INCREMENTAL_PARAMS.min_traditional,
              help='Minimal size of traditional representation')
@click.option('--classifier-format',
              default='expanded', show_default=True,
              type=click.Choice(x.name.lower() for x in ClassifierFormat),
              help='Classifier file format')
@click.option('--debug', is_flag=True, help='Show debug output')
def greet(
        max_entries, output_file, num_threads, result_dir,
        oi_cutoff, oi_algo: str, oi_bit_width, oi_only_exact,
        oi_use_actions: bool,
        lpm_max_groups, lpm_max_candidate_groups,
        lpm_max_expanded_bits: Optional[int],
        inc_add_fraction, inc_min_traditional, inc_output_file,
        inc_traditional_offline, classifier_format, debug
):  # pylint: disable=too-many-arguments
    global PARAMS             # pylint: disable=global-statement
    global OI_PARAMS          # pylint: disable=global-statement
    global LPM_PARAMS         # pylint: disable=global-statement
    global INCREMENTAL_PARAMS  # pylint: disable=global-statement

    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)

    PARAMS = GlobalParams(
        max_entries=max_entries,
        output_file=output_file,
        result_dir=result_dir,
        classifier_format=ClassifierFormat[classifier_format.upper()]
    )
    OI_PARAMS = OIParams(
        cutoff=oi_cutoff if oi_cutoff > 0 else None,
        algo=opt.OIAlgorithm[oi_algo.upper()],
        only_exact=oi_only_exact,
        bit_width=oi_bit_width,
        bit_range=None,
        use_actions=oi_use_actions,
    )
    LPM_PARAMS = LPMParams(
        max_groups=lpm_max_groups if lpm_max_groups > 0 else None,
        max_expanded_bits=lpm_max_expanded_bits,
        max_candidate_groups=lpm_max_candidate_groups if lpm_max_candidate_groups > 0 else None,
        oi_lpm_algo=LPM_PARAMS.oi_lpm_algo,
        oi_lpm_greedy_strategy=LPM_PARAMS.oi_lpm_greedy_strategy,
        oi_lpm_reduce_width_after=LPM_PARAMS.oi_lpm_reduce_width_after,
    )
    INCREMENTAL_PARAMS = IncrementalParams(
        min_traditional=inc_min_traditional,
        add_fraction_traditional=inc_add_fraction,
        output_file=inc_output_file,
        traditional_offline=inc_traditional_offline)

    if num_threads is not None:
        opt_native.set_number_of_threads(num_threads)

    print('Hey, we are gonna test some algos!!')


def do_optimize_oi(input_files, oi_params: OIParams):
    for input_file in input_files:
        print("performing {:s} on {:s}: {}, algo = {:s}".format(
            oi_params.kind, os.path.basename(input_file),
            f'bitwidth = {oi_params.bit_width:d}'
            if oi_params.bit_width is not None else '',
            oi_params.algo
        ))

        classifier = read_classifier(input_file)

        subclassifiers_infos, traditional = opt.decompose_oi(
            classifier, oi_params.bit_width, oi_params.algo, oi_params.cutoff,
            only_exact=oi_params.only_exact, bit_range=oi_params.bit_range,
            use_actions=oi_params.use_actions,
            max_oi_algo=opt.MaxOIAlgorithm.TOP_DOWN)

        if PARAMS.classifier_format == ClassifierFormat.IPS_UNIQUE:
            expanded_classifier = read_classifier(input_file, ClassifierFormat.EXPANDED)
            action_counter = Counter()
            action_counter.update(rule.action for rule in expanded_classifier)
            total_len = len(expanded_classifier)
            traditional_len = sum(action_counter[r.action] for r in traditional)
            subclassifiers_len = [
                sum(action_counter[_get_underlying_rule_action(r)]
                    for r in sc.classifier)
                for sc in subclassifiers_infos]
        else:
            total_len = len(classifier)
            traditional_len = len(traditional)
            subclassifiers_len = [
                    len(s.classifier) for s in subclassifiers_infos]

        add_row(oi_params.kind, os.path.basename(input_file), total_len,
                oi_params.algo, oi_params.bit_width, None,
                len(subclassifiers_infos), traditional_len,
                subclassifiers_len, None, None)

        io.save_oi_results(
                PARAMS.result_dir, oi_params, classifier,
                subclassifiers_infos, traditional)


def do_optimize_id(input_files):
    for input_file in input_files:
        print("performing id on {:s}".format(os.path.basename(input_file)))

        classifier = read_classifier(input_file)

        if PARAMS.classifier_format == ClassifierFormat.IPS_UNIQUE:
            expanded_classifier = read_classifier(input_file, ClassifierFormat.EXPANDED)
            action_counter = Counter()
            action_counter.update(rule.action for rule in expanded_classifier)
            total_len = len(expanded_classifier)
            traditional_len = sum(action_counter[r.action] for r in expanded_classifier)
        else:
            total_len = len(classifier)
            traditional_len = len(classifier)

        subclassifiers_len = []
        add_row('id', os.path.basename(input_file), total_len,
                None, None, None,
                len([]), traditional_len,
                subclassifiers_len, None, None)

        io.save_id_results(PARAMS.result_dir, classifier)


def _get_underlying_rule_action(r: AbstractVMREntry):
    if isinstance(r.action, FPCAction):
        return r.action.vmr_entry.action
    else:
        return r.action


def do_optimize_oi_lpm_joint(input_files, params: OILPMJointParams):
    for input_file in input_files:
        print("performing {:s} on {:s}: bitwidth = {:d}, algo = {:s}{:s}{:s}".format(
            params.kind, 
            os.path.basename(input_file), 
            params.oi.bit_width, 
            params.oi.algo,
            '' if params.lpm.max_groups is None else
            ', max_groups = {:d}'.format(params.lpm.max_groups),
            '' if params.lpm.max_expanded_bits is None else
            ', max_exp_bits = {:d}'.format(params.lpm.max_expanded_bits),
        ))

        classifier = read_classifier(input_file)

        max_groups = min(params.oi.cutoff, params.lpm.max_groups) \
            if params.lpm.max_groups is not None else params.oi.cutoff

        if params.lpm.max_expanded_bits is not None:
            subclassifiers, traditional, nexp_subclassifiers = opt.minimize_oi_lpm(
                classifier, oi_params.bit_width, oi_params.algo, max_groups,
                max_expanded_bits=lpm_params.max_expanded_bits,
                max_candidate_groups=lpm_params.max_candidate_groups,
                provide_non_expanded=True,
                max_oi_algo=opt.MaxOIAlgorithm.MIN_DEGREE)
            add_row(kind, os.path.basename(input_file), len(classifier),
                    oi_params.algo, oi_params.bit_width, None,
                    len(subclassifiers), len(traditional),
                    [len(s) for s in nexp_subclassifiers],
                    lpm_params.max_expanded_bits,
                    [len(s) for s in subclassifiers])
        else:
            subclassifiers, traditional = opt.minimize_oi_lpm(
                classifier, params.oi.bit_width, params.oi.algo, max_groups,
                use_actions=params.oi.use_actions,
                max_candidate_groups=params.lpm.max_candidate_groups,
                max_oi_algo=opt.MaxOIAlgorithm.TOP_DOWN)

            add_row(params.kind, os.path.basename(input_file), len(classifier),
                    params.oi.algo, params.oi.bit_width, None,
                    len(subclassifiers), len(traditional),
                    [len(s.classifier) for s in subclassifiers],
                    None, None)

            io.save_lpm_oi_joint_results(
                    PARAMS.result_dir,
                    params, classifier, subclassifiers, traditional)


def do_optimize_oi_lpm(input_files, oi_params, lpm_params):
    kind = 'oi_lpm'
    assert lpm_params.max_expanded_bits is None

    if lpm_params.max_groups is not None:
        kind += "_bounded"

    for input_file in input_files:
        print("performing {:s} on {:s}: bitwidth = {:d}, algo = {:s}{:s}".format(
            kind, os.path.basename(input_file), oi_params.bit_width, oi_params.algo,
            '' if lpm_params.max_groups is None else
            ', max_groups = {:d}'.format(lpm_params.max_groups),
        ))

        classifier = read_classifier(input_file)
        max_groups = min(oi_params.cutoff, lpm_params.max_groups) \
            if lpm_params.max_groups is not None else oi_params.cutoff
        _, traditional = opt.minimize_oi_lpm(
            classifier, oi_params.bit_width, oi_params.algo, max_groups,
            max_candidate_groups=lpm_params.max_candidate_groups)

        all_group_sizes = []

        if lpm_params.max_groups is not None:
            subclassifiers, traditionals = opt.maximize_coverage_bounded(subclassifiers, lpm_params.max_groups)

            add_row(kind, os.path.basename(input_file), len(classifier), oi_params.algo,
                    oi_params.bit_width, lpm_params.max_groups, len(subclassifiers),
                    len(traditionals) + sum(len(x) for x in traditionals), [len(s) for s in subclassifiers],
                    None, None)
        else:
            for subclassifier in subclassifiers:
                mgc = opt.minimize_num_groups(subclassifier)
                all_group_sizes.extend(len(s) for s in mgc)

            add_row(kind, os.path.basename(input_file), len(classifier), oi_params.algo,
                    oi_params.bit_width, None, len(all_group_sizes), len(traditionals), all_group_sizes,
                    None, None)


def do_optimize_lpm(input_files, lpm_params):
    kind = 'lpm' if lpm_params.max_groups is None else 'lpm_bounded'
    for input_file in input_files:
        max_groups_message = f": max_groups = {lpm_params.max_groups:d}" if lpm_params.max_groups is not None else ''
        print(f"performing lpm on {os.path.basename(input_file)}{max_groups_message}")

        classifier = read_classifier(input_file)

        if lpm_params.max_groups is not None:
            subclassifiers, traditionals = opt.maximize_coverage_bounded([classifier], lpm_params.max_groups)
        else:
            subclassifiers, traditionals = opt.minimize_num_groups(classifier), []

        add_row(kind, os.path.basename(input_file), len(classifier), None,
                classifier.bit_width, lpm_params.max_groups, len(subclassifiers),
                sum(len(x) for x in traditionals),
                [len(s) for s in subclassifiers],
                None, [len(s) for s in subclassifiers])


def do_optimize_lpm_w_oi(
        input_files, params: OIAndLPMParams
):
    for input_file in input_files:
        print("performing {:s} on {:s}{:s}{:s}".format(
            params.kind,
            os.path.basename(input_file),
            '' if params.lpm.max_groups is None else f': max_groups = {params.lpm.max_groups:d}',
            '' if params.oi.bit_width is None else f': bit_width = {params.oi.bit_width:d}'
        ))

        classifier = read_classifier(input_file)

        subclassifiers, traditional = opt.maximize_lpm_w_oi(
            classifier,
            max_num_groups=params.lpm.max_groups,
            max_width=params.oi.bit_width,
            algo=params.lpm.oi_lpm_algo,
            use_actions=params.oi.use_actions,
            greedy_strategy=params.lpm.oi_lpm_greedy_strategy,
            reduce_width_before_lpm=not params.lpm.oi_lpm_reduce_width_after,
            )

        add_row(params.kind, os.path.basename(input_file), len(classifier), None,
                params.oi.bit_width if params.oi.bit_width else classifier.bit_width,
                None, len(subclassifiers),
                len(traditional), [len(s.classifier) for s in subclassifiers],
                None, None)

        io.save_lpm_w_oi_results(
                PARAMS.result_dir,
                params, classifier, subclassifiers, traditional)


def do_optimize_lpm_oi(input_files, oi_params):
    for input_file in input_files:
        print("performing lpm_oi on {:s}: bitwidth = {:d}, algo = {:s}".format(
            os.path.basename(input_file), oi_params.bit_width, oi_params.algo,
        ))

        classifier = read_classifier(input_file)

        mgc = opt.minimize_num_groups(classifier)

        all_group_sizes = []
        size_traditional = 0
        for pr_classifier in mgc:
            subclassifiers, traditional = opt.decompose_oi(
                pr_classifier, oi_params.bit_width,
                oi_params.algo, max_num_groups=oi_params.cutoff,
                only_exact=False)
            all_group_sizes.extend(len(sc) for sc in subclassifiers)
            size_traditional += len(traditional)

        add_row('lpm_oi', os.path.basename(input_file), len(classifier), oi_params.algo,
                oi_params.bit_width, None, len(all_group_sizes), size_traditional, all_group_sizes)


def do_optimize_incremental(input_files, oi_params, lpm_params, inc_params):
    for input_file in input_files:
        print("performing incremental_updates on {:s}: bitwidth = {:d}, algo = {:s}".format(
            os.path.basename(input_file), oi_params.bit_width, oi_params.algo,
        ))
        classifier = read_classifier(input_file)

        max_groups = min(oi_params.cutoff, lpm_params.max_groups) \
            if lpm_params.max_groups is not None else oi_params.cutoff

        if inc_params.traditional_offline is None:
            _, traditional = opt.minimize_oi_lpm(
                classifier, oi_params.bit_width, oi_params.algo, max_groups,
                max_oi_algo=opt.MaxOIAlgorithm.MIN_DEGREE,
                max_candidate_groups=lpm_params.max_candidate_groups)
            num_traditional_offline = len(traditional)
        else:
            num_traditional_offline = inc_params.traditional_offline

        indices = list(range(len(classifier)))
        random.shuffle(indices)
        classifier = classifier.subset(indices)

        num_traditional = max(
            int((1.0 + inc_params.add_fraction_traditional)
                * num_traditional_offline),
            inc_params.min_traditional)

        incr_stats = opt.test_incremental(
            classifier, oi_params.bit_width, lpm_params.max_groups,
            num_traditional, max_oi_algo=opt.MaxOIAlgorithm.MIN_DEGREE,
            max_candidate_groups=lpm_params.max_candidate_groups)

        add_inc_row(
            os.path.basename(input_file), len(classifier),
            oi_params.bit_width, lpm_params.max_groups,
            inc_params.add_fraction_traditional, num_traditional,
            len(incr_stats),
            total_traditional=sum(x.num_traditional for x in incr_stats),
            total_lpm=sum(x.num_in_groups for x in incr_stats)
        )


@greet.command()
@click.option('--oi-start-bit', default=None, type=int,
              help='Position of the first bit used for OI')
@click.option('--oi-end-bit', default=None, type=int,
              help='Position of the last bit used for OI')
@click.argument('input_files', nargs=-1)
def optimize_oi(
        input_files,
        oi_start_bit: Optional[int],
        oi_end_bit: Optional[int]
):
    if (oi_start_bit is None) != (oi_end_bit is None):
        print('Either both start and end bits must be specified or none',
              file=sys.stderr)
        sys.exit(1)

    do_optimize_oi(
        input_files,
        OI_PARAMS._replace(
            bit_range=_bit_pos_to_bitrange(oi_start_bit, oi_end_bit)
        ))


def _bit_pos_to_bitrange(
        oi_start_bit: Optional[int],
        oi_end_bit: Optional[int]
) -> Optional[opt.BitRange]:
    if oi_start_bit is None or oi_end_bit is None:
        return None
    return opt.BitRange(oi_start_bit - 1, oi_end_bit)


@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_oi_lpm(input_files):
    do_optimize_oi_lpm(input_files, OI_PARAMS, LPM_PARAMS)


@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_oi_lpm_joint(input_files):
    do_optimize_oi_lpm_joint(
            input_files,
            OILPMJointParams(oi=OI_PARAMS, lpm=LPM_PARAMS))


@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_lpm(input_files):
    do_optimize_lpm(input_files, LPM_PARAMS)


@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_lpm_oi(input_files):
    do_optimize_lpm_oi(input_files, OI_PARAMS)

@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_id(input_files):
    do_optimize_id(input_files)


@greet.command()
@click.argument('input_files', nargs=-1)
@click.option('--oi-lpm-algo', required=True, 
              type=click.Choice(x.name.lower() for x in opt.LPMOIAlgorithm),
              help='OI LPM algorithm to use')
@click.option('--oi-lpm-greedy-strategy', 
              type=click.Choice(x.name.lower() for x in opt.GreedyStrategy),
              help='OI LPM algorithm to use')
@click.option('--oi-lpm-reduce-width-after', is_flag=True, help='run OI after')
def optimize_lpm_w_oi(
        input_files,
        oi_lpm_algo,
        oi_lpm_greedy_strategy,
        oi_lpm_reduce_width_after,
):
    do_optimize_lpm_w_oi(
            input_files,
            OIAndLPMParams(
                lpm=LPM_PARAMS._replace(
                    oi_lpm_algo=opt.LPMOIAlgorithm[oi_lpm_algo.upper()],
                    oi_lpm_greedy_strategy=opt.GreedyStrategy[
                        oi_lpm_greedy_strategy.upper()
                    ] if oi_lpm_greedy_strategy is not None else None,
                    oi_lpm_reduce_width_after=oi_lpm_reduce_width_after,
                ),
                oi=OI_PARAMS))


@greet.command()
@click.argument('input_files', nargs=-1)
def optimize_incremental(input_files):
    do_optimize_incremental(
        input_files, OI_PARAMS, LPM_PARAMS, INCREMENTAL_PARAMS)


@greet.command()
@click.argument('input_files', nargs=-1)
@click.option('--bit-width', type=int, multiple=True,
              help='Required bit width')
@click.option('--max-expanded-bits', type=int, multiple=True,
              help='Maximal number of expanded bits')
@click.option('--without-oi', help='Disable OI calculation', is_flag=True)
@click.option('--without-lpm', help='Disable LPM calculation', is_flag=True)
@click.option('--without-oi-lpm', is_flag=True,
              help='Disable OI-LPM calculation')
@click.option('--without-oi-exact', is_flag=True,
              help='Disable OI exact calculation')
@click.option('--without-oi-lpm-exp', is_flag=True,
              help='Disable OI-LPM with expansions calculation')
def optimize_for_paper(
        input_files, bit_width, max_expanded_bits, without_oi,
        without_lpm, without_oi_lpm, without_oi_exact, without_oi_lpm_exp
):  # pylint: disable=too-many-arguments
    if not without_lpm:
        do_optimize_lpm(input_files, LPM_PARAMS)
    for bw in bit_width:
        if not without_oi_lpm:
            do_optimize_oi_lpm_joint(
                input_files,
                OI_PARAMS._replace(bit_width=bw),
                LPM_PARAMS)
        if not without_oi_lpm_exp:
            for eb in max_expanded_bits:
                do_optimize_oi_lpm_joint(
                    input_files,
                    OI_PARAMS._replace(bit_width=bw),
                    LPM_PARAMS._replace(max_expanded_bits=eb))


if __name__ == '__main__':
    greet()
