import logging
import dataclasses
import os
import json
import shutil
from typing import Sequence

from cls.classifiers.abstract import AbstractBasicClassifier
from cls.classifiers.simple import ReorderingClassifier
from cls.vmrs.simple import FPCAction
from cls.optimizations.oi_lpm import GroupInfo
from cls.vmrs.filter import Filter
from cls.classifiers.meta import MatchType, Meta, TableMeta

from predicat_runner.common import *


def save_oi_results(
        result_dir: str,
        oi_params: OIParams,
        original_classifier: AbstractBasicClassifier,
        subclassifiers: Sequence[GroupInfo],
        traditional_part: AbstractBasicClassifier,
):
    save_results(
            result_dir,
            _create_oi_method_path_name(oi_params),
            original_classifier,
            subclassifiers,
            traditional_part,
            MatchType.EXACT if oi_params.only_exact else MatchType.TERNARY)

def save_id_results(
        result_dir: str,
        original_classifier: AbstractBasicClassifier,
):
    save_results(
            result_dir,
            'id',
            original_classifier,
            [],
            original_classifier,
            MatchType.TERNARY)


def save_lpm_w_oi_results(
        result_dir: str,
        params: OIAndLPMParams,
        original_classifier: AbstractBasicClassifier,
        subclassifiers: Sequence[GroupInfo],
        traditional_part: AbstractBasicClassifier,
):
    save_results(
            result_dir,
            _create_lpm_w_oi_method_path_name(params),
            original_classifier,
            subclassifiers,
            traditional_part,
            MatchType.LPM)


def save_lpm_oi_joint_results(
        result_dir: str,
        params: OILPMJointParams,
        original_classifier: AbstractBasicClassifier,
        subclassifiers: Sequence[GroupInfo],
        traditional_part: AbstractBasicClassifier,
):
    save_results(
            result_dir,
            _create_lpm_oi_joint_method_path_name(params),
            original_classifier,
            subclassifiers,
            traditional_part,
            MatchType.LPM)


def _create_lpm_oi_joint_method_path_name(params: OILPMJointParams) -> str:
    return '.'.join([params.kind] + _optional(str(params.lpm.max_groups))
                    + _optional(str(params.oi.bit_width)))


def _create_oi_method_path_name(oi_params: OIParams) -> str:
    return '.'.join([oi_params.kind] + _optional(str(oi_params.bit_width))
                    + [oi_params.algo.name])


def _create_lpm_w_oi_method_path_name(params: OIAndLPMParams) -> str:
    return '.'.join([params.kind] + _optional(str(params.lpm.max_groups))
                    + _optional(str(params.oi.bit_width)))


def save_results(
        result_dir: str,
        method_path_name: str,
        original_classifier: AbstractBasicClassifier,
        subclassifiers: Sequence[GroupInfo],
        traditional_part: AbstractBasicClassifier,
        sub_match_type: MatchType,
):
    results_path = os.path.join(
            result_dir, method_path_name, original_classifier.name)

    print(f'Saving result to {results_path}')

    if os.path.exists(results_path):
        shutil.rmtree(results_path)
    os.makedirs(results_path)

    meta = Meta(groups=[], tcam=None)

    for i, g in enumerate(subclassifiers):
        group_file_name = f'group_{i}.tsv'
        _save_classifier_to_file(
                g.classifier, os.path.join(results_path, group_file_name))
        orig_group_file_name = f'orig_{group_file_name}'
        _save_classifier_to_file(
                original_classifier.subset(g.indices),
                os.path.join(results_path, orig_group_file_name))
        meta.groups.append(_create_meta(
            g.classifier,
            group_file_name,
            orig_group_file_name,
            sub_match_type))

    tcam_file_name = 'tcam.tsv'
    _save_classifier_to_file(
            traditional_part, os.path.join(results_path, tcam_file_name))
    meta = dataclasses.replace(
            meta,
            tcam=_create_meta(
                traditional_part,
                tcam_file_name,
                tcam_file_name,
                MatchType.TERNARY))

    with open(os.path.join(results_path, 'meta.json'),
              'w',
              encoding='utf-8') as meta_file:
        json.dump(meta.to_json(), meta_file, indent=2)


def _save_classifier_to_file(
        classifier: AbstractBasicClassifier,
        file_name: str
):
    non_identified_entry_actions = 0

    with open(file_name, 'w') as group_file:
        for entry in classifier.vmr:
            action = _get_action_entry_action(entry.action)
            if action is None:
                non_identified_entry_actions += 1
                continue
            print(Filter.from_vmr_entry(entry),
                  action, file=group_file, sep='\t')

    if non_identified_entry_actions != 0:
        logging.warning(f'actions of {non_identified_entry_actions} entries were not identified')


def _get_action_entry_action(action):
    if isinstance(action, int):
        return action
    if isinstance(action, FPCAction):
        return action.vmr_entry.action
    return None


def _create_meta(
        classifier: AbstractBasicClassifier,
        group_file_name: str,
        orig_group_file_name: str,
        match_type: MatchType,
        ) -> TableMeta:
    meta_bits = None
    logging.debug(f'writing metadata for {type(classifier)}')
    if isinstance(classifier, ReorderingClassifier):
        meta_bits = classifier.bits
    return TableMeta(
            path=group_file_name,
            orig_path=orig_group_file_name,
            bits=meta_bits,
            match_type=match_type)


def _optional(x) -> Sequence:
    return [x] if x is not None else []
