{
  description = "Environment for testing lpm";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        python = pkgs.python3.withPackages (ps:
          with ps; [
            click
            bitstring
            numpy
            pandas
            matplotlib
            pyqt5
            pylint
            mypy
            autopep8
            python-lsp-server
            pylsp-mypy
          ]);
        switchd_shell = import ../p4_predicat/switchd.nix;
      in {
        devShell = pkgs.mkShell {
          buildInputs = [ python pkgs.snakemake pkgs.yapf ];
          shellHook = ''
            export PYTHONPATH=$PYTHONPATH:"../../classifiers-lib/:../../classifiers-lib/p4t_native/cmake-build-release/p4t";
            export PREDICAT_PLOTS_DIR=/home/pschuprikov/Dropbox/lpm-n-order-independence/journal/newgraphs
            export NIXOPS_DEPLOYMENT=lpm
            export PREDICAT_EXE=../../p4c/cmake-build-release/backends/predicat/predicat
            export TOFINO_MODEL_EXE=/home/pschuprikov/work/usi/bf-sde-9.9.0/install/bin/tofino-model
            export SWITCHD_EXE=/home/pschuprikov/work/usi/bf-sde-9.9.0/install/bin/bf_switchd
            export BF_P4C_COMPILER="/home/pschuprikov/work/usi/bf-sde-9.9.0/install/bin/bf-p4c"
            export BF_SDE="/home/pschuprikov/work/usi/bf-sde-9.5.1"
          '';
        };
        packages = { };
      });
}
