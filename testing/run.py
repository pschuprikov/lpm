import asyncio
import dataclasses
import json
import logging
import os
import shelve
import csv
from asyncio import Future, Semaphore, ensure_future, create_task, Lock
from itertools import product
from os import path
from shelve import Shelf
import subprocess
from subprocess import TimeoutExpired, CalledProcessError
from typing import Awaitable, Iterable, NamedTuple, Optional, Sequence, cast, IO

import click
import yaml

import lib
import tofino
from run_caching import Cache, cached

RESULTS_DIR: str = 'results'


class Global:
    dry_run = False
    bootstrap = False
    cache: Optional[Cache] = None
    num_processes: int = 1
    semaphore_ = None
    test_lock_ = None

    @classmethod
    def set_num_processes(cls, num_processes: int):
        cls.num_processes = num_processes

    @classmethod
    def semaphore(cls) -> Semaphore:
        semaphore = cls.semaphore_ or Semaphore(cls.num_processes)
        cls.semaphore_ = semaphore
        return semaphore

    @classmethod
    def test_lock(cls) -> Lock:
        test_lock = cls.test_lock_ or Lock()
        cls.test_lock_ = test_lock
        return test_lock

    @classmethod
    def get_cache(cls):
        return cls.cache


@click.command
@click.option('--config', required=True, type=click.File('r'))
@click.option('--num-processes', default=1, type=int)
@click.option('--dry-run', is_flag=True)
@click.option('--no-cache', is_flag=True)
@click.option('--max-entries', type=int)
@click.option('--plots-dir', type=str, envvar='PREDICAT_PLOTS_DIR')
@click.option('--bootstrap', is_flag=True)
@click.option('--debug-caching', is_flag=True)
def main(config, num_processes, dry_run, bootstrap, no_cache, 
         max_entries: Optional[int], plots_dir: Optional[str], debug_caching):
    logging.basicConfig(level=logging.INFO)
    Cache.logger.setLevel(logging.WARN if not debug_caching else logging.INFO)
    Global.dry_run = dry_run
    Global.bootstrap = bootstrap
    Global.set_num_processes(num_processes)

    async_to_run = predicat(yaml.load(config, yaml.Loader), max_entries,
                            plots_dir)
    # async_to_run = simple_test()

    if not no_cache and not dry_run:
        with shelve.open('.shelf') as shelf:
            Global.cache = Cache(cast(Shelf, shelf))
            asyncio.run(async_to_run)
    else:
        asyncio.run(async_to_run)


class RunConfig(NamedTuple):
    cls: lib.Classifier
    method: lib.Method
    width: Optional[int]
    max_entries: Optional[int]

    async def run(self) -> lib.OptimizationResult:
        if self.method is lib.Method.EXACT:
            return await optimize_exact(self.cls, self.max_entries)
        if self.method is lib.Method.BASIC and self.width is None:
            return await optimize_basic(self.cls, self.max_entries)
        if self.width is None:
            raise AssertionError()
        if self.method is lib.Method.OI_LPM_JOINT:
            return await optimize_oi_lpm_joint(self.cls, self.width,
                                               self.max_entries)
        if self.method is lib.Method.LPM_W_OI:
            return await optimize_lpm_oi(self.cls, self.width,
                                         self.max_entries)
        if self.method is lib.Method.OI:
            return await optimize_oi(self.cls, self.width, self.max_entries)
        raise AssertionError()


class BuildOutputWithInfo(NamedTuple):
    run_config: RunConfig
    build_output: lib.BuiltP4Program
    trans: Optional[lib.Transformation]


async def simple_test():
    return await optimize_lpm_oi(
            lib.Classifier(name='acl1'),
            lib.FULL_WIDTH,
            None)


async def predicat(config, max_entries: Optional[int], plots_dir: Optional[str]):
    plots = []
    for width, arch_str in product(config['width'], config['arch']):
        arch = lib.Arch[arch_str.upper()]
        basic_analysis = create_task(
            analyze_basic(config, arch, max_entries),
            name='analyze_basic') if width is None else None
        for a in analyze(config, width, arch, max_entries):
            for v in lib.Variable:
                for trans in get_transformations_from_config(config):
                    plots.append(plot(a, basic_analysis, trans, v, plots_dir))
    return await asyncio.gather(*plots)


def analyze(config, width: Optional[int], arch: lib.Arch,
            max_entries: Optional[int]) -> list[Future[lib.Analysis]]:
    analysis: list[Future[lib.Analysis]] = []
    for bound_kind, bound_val in get_bounds_from_config(config):
        analysis.append(
            ensure_future(
                analyze_bound_kind(config, bound_kind, bound_val, arch, width,
                                   max_entries)))
    return analysis


async def analyze_basic(config, arch: lib.Arch,
                        max_entries: Optional[int]) -> lib.Analysis:
    build_outputs: list[Awaitable[BuildOutputWithInfo]] = []
    for run_config in gen_valid_basic_configs(config, max_entries):
        opt_result = create_task(run_config.run(),
                                 name=f'run_{run_config.method.name.lower()}')
        build_outputs.append(
            process_program(config, run_config, opt_result, None, arch))
    return await analyze_basic_setting(arch, await
                                       asyncio.gather(*build_outputs))


async def analyze_result(
    config, arch: lib.Arch, width: int,
    opt_result_fut: Future[lib.OptimizationResult]
) -> Optional[lib.ResourceChoice]:
    opt_result = await opt_result_fut
    meta: dict = opt_result.load_meta()
    groups = meta['groups']

    analyzed_groups = groups[:10]
    result = lib.ResourceChoice(opt_result, [])

    has_failed = False
    for i in range(len(analyzed_groups)):
        trial_result = await create_task(
            try_different_resources(config, arch, width, opt_result_fut,
                                    groups, i),
            name=f'Analyzing {opt_result.base_dir} {i}')
        good_ones = sorted(k for k, v in trial_result.items() if v is not None)
        if len(good_ones) == 0:
            has_failed = True
        else:
            result.resources_per_group.append(good_ones[0])

    if has_failed:
        return None

    _makedirs(path.dirname(result.path), exist_ok=True)
    result.persist()
    logging.info(f'Resource choice saved to {result.path}')
    return result


async def try_different_resources(
    config, arch: lib.Arch, width: int,
    opt_result_fut: Future[lib.OptimizationResult], groups: dict,
    group_idx: int
) -> dict[lib.TestResources, Optional[tofino.TableSizeResult]]:
    task_logger().info(f'try different resources for group #{group_idx}')
    opt_result = await opt_result_fut
    good_ones = set()
    result = {}
    actual_size = _get_group_size(opt_result, groups[group_idx])
    for resources in gen_test_resources(config['resources'], actual_size):
        if actual_size > resources.size:
            continue
        if _has_fewer_eq(good_ones, resources):
            continue
        test = ensure_future(
            prepare_p4_test(config, lib.TestConfig(width, arch, resources)))
        test_result = await run_p4_test(opt_result_fut, group_idx, test)
        if test_result is not None:
            good_ones.add(resources)
        result[resources] = test_result
    return result


def _has_fewer_eq(elems: Iterable[lib.TestResources],
                  other: lib.TestResources) -> bool:
    for elem in elems:
        if elem.size <= other.size and elem.number_partitions <= other.number_partitions:
            return True
    return False


def save_resource_trials(results: dict[lib.TestResources,
                                       Optional[tofino.TableSizeResult]],
                         fname: str):
    task_logger().info(f'saving trials to {fname}')
    with open(fname, 'w') as csvfile:
        writer = csv.DictWriter(csvfile,
                                lib.TestResources._fields +
                                tofino.TableSizeResult._fields,
                                delimiter='\t')
        writer.writeheader()
        for resource, result in results.items():
            if result is None:
                writer.writerow(resource._asdict())
            else:
                writer.writerow(resource._asdict() | result._asdict())


def gen_test_resources(config, actual_size: Optional[int] = None):
    extra_sizes = [
        actual_size
    ] if actual_size is not None and config['use_actual_size'] else []
    nps = sorted(int(x) for x in config['num_partitions'])
    sizes = sorted(int(x) for x in config['size'] + extra_sizes)
    for np in nps:
        for size in sizes:
            yield lib.TestResources(np, size)


async def prepare_p4_test(config,
                          test_cfg: lib.TestConfig) -> lib.BuiltP4Program:
    build_dir = path.join(RESULTS_DIR, test_cfg.get_path, 'build')
    install_dir = tofino.sde_install_dir()
    test_program = ensure_future(
        _get_test_p4_program(test_cfg.width, test_cfg.arch))
    return await ensure_future(
        build_p4(
            ensure_future(
                configure_p4_inside_dir(
                    test_program,
                    True,
                    build_dir,
                    install_dir,
                    extra_p4pp_flags=
                    f'-DPREDICAT_NUMBER_PARTITIONS={test_cfg.resources.number_partitions} -DPREDICAT_SIZE={test_cfg.resources.size}'
                )), config['p4-build-timeout']))


async def _get_test_p4_program(width: int, arch: lib.Arch) -> lib.P4Program:
    return lib.P4Program(
        path.join('..', 'p4_predicat', f'p4_predicat_test_{width}.p4'), arch)


@cached(Global.get_cache, 
        force=lambda _opt, idx, _test: idx == 0)
async def run_p4_test(
    opt_result_fut: Future[lib.OptimizationResult],
    group_idx: int,
    test_program_fut: Future[lib.BuiltP4Program]
) -> Optional[tofino.TableSizeResult]:
    await test_program_fut
    opt_result = await opt_result_fut
    meta: dict = opt_result.load_meta()
    async with Global.test_lock():
        installed_test_program = await install_p4(test_program_fut)
        run_dir = path.join(installed_test_program.build_dir, '..', 'run')
        _makedirs(run_dir, exist_ok=True)
        async with tofino.tofino_model(installed_test_program, run_dir):
            async with tofino.switchd(installed_test_program, run_dir):
                group_path = opt_result.get_group_path(
                    meta['groups'][group_idx])
                task_logger().info(f'installing classifier from {group_path}')
                try:
                    tofino.run_bf_python(
                        path.abspath('install_classifier.py') + ' ' +
                        group_path)
                except CalledProcessError as ex:
                    if ex.returncode == 1:
                        return None
                return await tofino.get_table_size()


async def plot(analysis_fut: Future[lib.Analysis],
               basic_analysis_fut: Optional[Future[lib.Analysis]],
               trans: lib.Transformation, var: lib.Variable,
               plots_dir: Optional[str]) -> lib.Plot:
    analysis = await analysis_fut
    basic_analysis = await basic_analysis_fut if basic_analysis_fut is not None else None

    analysis_relative_path = os.path.relpath(path.dirname(analysis.path),
                                             path.abspath(RESULTS_DIR))

    output_path = path.join(
        plots_dir or RESULTS_DIR, analysis_relative_path,
        trans.key_method.name.lower(), f'{trans.combiner}_combiner'.lower(),
        var.name.lower(),
        path.splitext(path.basename(analysis.path))[0] + '.pdf')
    _makedirs(path.dirname(output_path), exist_ok=True)
    args = [
        'python', '../p4_predicat/p4-analyze.py', 'plot',
        var.name.lower(),
        trans.key_method.name.lower(),
        str(trans.combiner), analysis.path,
        basic_analysis.path if basic_analysis is not None else None,
        output_path
    ]
    await _run_process_and_log(args)
    return lib.Plot(output_path)


async def analyze_bound_kind(config, bound_kind: lib.BoundKind, bound_val: int,
                             arch: lib.Arch, width: Optional[int],
                             max_entries: Optional[int]) -> lib.Analysis:
    build_outputs: list[Awaitable[BuildOutputWithInfo]] = []
    for run_config in gen_valid_configs(config, width, max_entries):
        opt_result = create_task(run_config.run(),
                                 name=f'run_{run_config.method.name.lower()}')
        for trans in get_bound_transformations_from_config(
                bound_kind, bound_val, config):
            build_outputs.append(
                process_program(config, run_config, opt_result, trans, arch))
    return await analyze_setting(bound_kind, bound_val, width, arch, await
                                 asyncio.gather(*build_outputs))


class P4GenerationConfig:

    async def generate_p4(self,
                          original_program: lib.P4Program) -> lib.P4Program:
        raise NotImplementedError


@dataclasses.dataclass
class OptimizedP4GenerationConfig(P4GenerationConfig):
    opt_result: Future[lib.OptimizationResult]
    resources: Optional[Future[Optional[lib.ResourceChoice]]]
    trans: lib.Transformation

    async def generate_p4(self,
                          original_program: lib.P4Program) -> lib.P4Program:
        return await generate_p4(self.opt_result, self.resources,
                                 original_program, original_program.arch,
                                 self.trans.bound_kind, self.trans.bound_val,
                                 self.trans)


@dataclasses.dataclass
class BasicP4GenerationConfig(P4GenerationConfig):
    opt_result: Future[lib.OptimizationResult]

    async def generate_p4(self,
                          original_program: lib.P4Program) -> lib.P4Program:
        return await generate_p4_basic(self.opt_result, original_program,
                                       original_program.arch)


@dataclasses.dataclass
class SingleGroupP4GenerationConfig(P4GenerationConfig):
    opt_result: Future[lib.OptimizationResult]
    group_idx: int

    async def generate_p4(self,
                          original_program: lib.P4Program) -> lib.P4Program:
        return await generate_p4_group(self.opt_result, original_program,
                                       original_program.arch, self.group_idx)


def process_program(config, run_config: RunConfig,
                    opt_result: Future[lib.OptimizationResult],
                    trans: Optional[lib.Transformation],
                    arch: lib.Arch) -> Awaitable[BuildOutputWithInfo]:
    if trans is None:
        generation_config = BasicP4GenerationConfig(opt_result)
    else:
        resources = None
        if run_config.width is not None:
            resources = create_task(analyze_result(config, arch, 
                                                   run_config.width,
                                                   opt_result), 
                                    name='analyze_result')
        generation_config = OptimizedP4GenerationConfig(opt_result, resources, trans)

    return process_program_config(config, run_config, trans, arch,
                                  generation_config)


def process_program_config(
        config, run_config: RunConfig, trans: Optional[lib.Transformation],
        arch: lib.Arch, generation_config: P4GenerationConfig
) -> Awaitable[BuildOutputWithInfo]:
    original_program = _get_original_program(arch)
    transformed = ensure_future(
        generation_config.generate_p4(original_program))
    built_program = ensure_future(
        build_p4(ensure_future(configure_p4(transformed)),
                 config['p4-build-timeout']))
    return build_output_to_info(run_config, built_program, trans)


def _get_original_program(arch: lib.Arch) -> lib.P4Program:
    return lib.P4Program(path.join('..', 'p4_predicat', 'p4_predicat.p4'),
                         arch)


async def build_output_to_info(
        run_config: RunConfig, build_output: Future[lib.BuiltP4Program],
        trans: Optional[lib.Transformation]) -> BuildOutputWithInfo:
    return BuildOutputWithInfo(run_config, await build_output, trans)


def get_transformations_from_config(config) -> Iterable[lib.Transformation]:
    for bound_kind, bound_val in get_bounds_from_config(config):
        yield from get_bound_transformations_from_config(
            bound_kind, bound_val, config)


def get_bound_transformations_from_config(
        bound_kind, bound_val, config) -> Iterable[lib.Transformation]:
    for key_method in config['key_method']:
        for combiner in config['combiner']:
            yield lib.Transformation(bound_kind, bound_val,
                                     lib.KeyMethod[key_method.upper()],
                                     combiner)


def get_bounds_from_config(config) -> Iterable[tuple[lib.BoundKind, int]]:
    for max_groups in config['max_groups']:
        yield lib.BoundKind.MAX_GROUPS, max_groups
    for fraction in config['percentage']:
        yield lib.BoundKind.PERCENTAGE, fraction


@cached(Global.get_cache, version=1)
async def configure_p4(
        program_fut: Future[lib.P4Program]) -> lib.ConfiguredP4Program:
    program = await program_fut
    build_dir = path.abspath(
        path.join(path.dirname(program.path), '..', 'build'))
    return await configure_p4_inside_dir(program_fut, False, build_dir)


@cached(Global.get_cache, version=1)
async def configure_p4_inside_dir(
    program_fut: Future[lib.P4Program],
    link: bool,
    build_dir: str,
    install_dir: Optional[str] = None,
    extra_p4pp_flags: Optional[str] = None,
) -> lib.ConfiguredP4Program:
    program = await program_fut
    _makedirs(build_dir, exist_ok=True)
    env = {}

    env['P4PPFLAGS'] = extra_p4pp_flags if extra_p4pp_flags is not None else ''

    if not link:
        env['P4FLAGS'] = '--no-link'

    args = lib.create_nix_shell_args(
        lib.p4_predicat_dir(),
        f'configurePhase',
        attr_path='compileShell',
        str_args={
            'name': program.basename,
            'arch': program.arch.p4_arch,
            'path': path.abspath(path.dirname(program.path)),
        } | ({
            'prefix': install_dir
        } if install_dir is not None else {}),
        env=env)

    await _run_process_and_log(args, cwd=build_dir, stdout=subprocess.DEVNULL)
    return lib.ConfiguredP4Program.from_p4_program(program, build_dir,
                                                   install_dir)


@cached(Global.get_cache, version=2)
async def build_p4(program_fut: Future[lib.ConfiguredP4Program],
                   timeout: str) -> lib.BuiltP4Program:
    program = await program_fut
    task_logger().info(
        f'building inside {program.build_dir} with timeout {timeout}')
    log_filename = path.abspath(path.join(program.build_dir, '..',
                                          'build.log'))

    args = lib.create_nix_shell_args(lib.p4_predicat_dir(),
                                     'make clean && make',
                                     attr_path='compileShell',
                                     str_args={
                                         'name': program.basename,
                                         'arch': program.arch.p4_arch
                                     })

    result = lib.BuiltP4Program.from_configured_p4_program(program)
    try:
        with open(log_filename, 'w') as log_filename:
            await _run_process_and_log(args,
                                       cwd=program.build_dir,
                                       timeout=timeout,
                                       stderr=log_filename)
    except TimeoutExpired:
        _write_to_file(result.manifest, json.dumps({'error': 'timeout'}))
    except CalledProcessError as e:
        _write_to_file(result.manifest,
                       json.dumps({'error': {
                           'nonzero-return': e.returncode
                       }}))

    return result


async def install_p4(
        built_program_fut: Future[lib.BuiltP4Program]
) -> lib.InstalledP4Program:
    built_program = await built_program_fut
    task_logger().info(
        f'installing {built_program.build_dir} to {built_program.install_dir}')
    args = lib.create_nix_shell_args(lib.p4_predicat_dir(),
                                     'make install',
                                     attr_path='compileShell',
                                     str_args={
                                         'name': built_program.basename,
                                         'arch': built_program.arch.p4_arch
                                     })

    await _run_process_and_log(args,
                               cwd=built_program.build_dir,
                               stdout=subprocess.DEVNULL,
                               stderr=subprocess.DEVNULL)
    return lib.InstalledP4Program.from_built_p4_program(built_program)


@cached(Global.get_cache)
async def optimize_oi(cls: lib.Classifier, width: int,
                      max_entries: Optional[int]) -> lib.OptimizationResult:
    args = [
        'python', '-m', 'predicat_runner.checker', '--oi-use-actions',
        '--oi-cutoff', '0', '--oi-bit-width',
        str(width)
    ]

    if max_entries is not None:
        args.extend(['--max-entries', str(max_entries)])

    args.extend(['optimize-oi', cls.path])

    await _run_process_and_log(args)
    return lib.OptimizationResult(
        cls,
        path.join(RESULTS_DIR, lib.METHOD_TEMPLATES['oi'].format(width=width)))


def gen_valid_configs(config, width: Optional[int],
                      max_entries: Optional[int]) -> Iterable[RunConfig]:
    for cls, method_str in product(classifiers_from_config(config),
                                   config['method']):
        method = lib.Method[method_str.upper()]
        if method is lib.Method.EXACT and width is not None:
            yield RunConfig(cls, method, None, max_entries)
        elif method is not lib.Method.BASIC and width is None:
            yield RunConfig(cls, method, lib.FULL_WIDTH, max_entries)
        else:
            yield RunConfig(cls, method, width, max_entries)


def gen_valid_basic_configs(config, max_entries: Optional[int]) -> Iterable[RunConfig]:
    for cls in classifiers_from_config(config):
        yield RunConfig(cls, lib.Method.BASIC, None, max_entries)


def classifiers_from_config(config) -> Iterable[lib.Classifier]:
    for cls_str in config['cls']:
        yield lib.Classifier(cls_str)


async def analyze_setting(
        bound_kind: lib.BoundKind, bound_val: int, width: Optional[int],
        arch: lib.Arch,
        build_outputs: Sequence[BuildOutputWithInfo]) -> lib.Analysis:
    output_csv = path.join(RESULTS_DIR, f'{bound_kind.old_name}_{bound_val}',
                           f'stat_{width}_{arch.name.lower()}.csv')
    for build in build_outputs:
        assert build.trans is not None
        assert build.trans.bound_kind == bound_kind
        assert build.trans.bound_val == bound_val

    _makedirs(path.dirname(output_csv), exist_ok=True)
    args = ['python', '../p4_predicat/p4-analyze.py', 'analyze'
            ] + construct_analyze_per_build_args(build_outputs) + [
                '--output', output_csv
            ]

    await _run_process_and_log(args)

    return lib.Analysis(lib.HashedPath(output_csv))


def construct_analyze_per_build_args(
        build_outputs: Sequence[BuildOutputWithInfo]):
    per_build_args = []

    for build in build_outputs:
        if build.trans is not None:
            per_build_args.extend([
                '--pipe', build.run_config.cls.name,
                build.run_config.method.name.lower(),
                str(build.run_config.width),
                build.trans.bound_kind.name.lower(),
                str(build.trans.bound_val),
                build.trans.key_method.name.lower(),
                str(build.trans.combiner).lower(), build.build_output.manifest
            ])
        else:
            per_build_args.extend([
                '--basic-pipe', build.run_config.cls.name,
                build.run_config.method.name.lower(),
                str(build.run_config.width), build.build_output.manifest
            ])

    return per_build_args


async def analyze_basic_setting(
        arch: lib.Arch,
        build_outputs: Sequence[BuildOutputWithInfo]) -> lib.Analysis:
    output_csv = path.join(RESULTS_DIR, f'stat_basic_{arch.name.lower()}.csv')
    for build in build_outputs:
        assert build.trans is None

    _makedirs(path.dirname(output_csv), exist_ok=True)

    args = ['python', '../p4_predicat/p4-analyze.py', 'analyze'
            ] + construct_analyze_per_build_args(build_outputs) + [
                '--output', output_csv
            ]

    await _run_process_and_log(args)

    return lib.Analysis(lib.HashedPath(output_csv))


@cached(Global.get_cache)
async def optimize_exact(cls: lib.Classifier, max_entries: Optional[int]):
    args = [
        'python',
        '-m',
        'predicat_runner.checker',
        '--oi-cutoff',
        '100',
        '--oi-only-exact',
        '--oi-use-actions',
        '--lpm-max-candidate-groups',
        '0',
    ]

    if max_entries is not None:
        args.extend(['--max-entries', str(max_entries)])
    args.extend(['optimize-oi', cls.path])
    await _run_process_and_log(args)
    return lib.OptimizationResult(
        cls, path.join(RESULTS_DIR, lib.METHOD_TEMPLATES['exact']))


@cached(Global.get_cache)
async def optimize_basic(cls: lib.Classifier, max_entries: Optional[int]):
    args = [
        'python',
        '-m',
        'predicat_runner.checker',
    ]

    if max_entries is not None:
        args.extend(['--max-entries', str(max_entries)])
    args.extend(['optimize-id', cls.path])
    await _run_process_and_log(args)
    return lib.OptimizationResult(
        cls, path.join(RESULTS_DIR, lib.METHOD_TEMPLATES['basic']))


@cached(Global.get_cache)
async def optimize_oi_lpm_joint(cls: lib.Classifier, width: int,
                                max_entries: Optional[int]):
    args = [
        'python', '-m', 'predicat_runner.checker', '--oi-use-actions',
        '--lpm-max-candidate-groups', '0', '--oi-bit-width',
        str(width)
    ]

    if max_entries is not None:
        args.extend(['--max-entries', str(max_entries)])
    args.extend(['optimize-oi-lpm-joint', cls.path])
    await _run_process_and_log(args)
    return lib.OptimizationResult(
        cls,
        path.join(RESULTS_DIR,
                  lib.METHOD_TEMPLATES['oi_lpm_joint'].format(width=width)))


@cached(Global.get_cache, version=1)
async def optimize_lpm_oi(cls: lib.Classifier, width: int,
                          max_entries: Optional[int]):
    args = [
        'python', '-m', 'predicat_runner.checker', '--oi-use-actions',
        '--lpm-max-candidate-groups', '0', '--oi-bit-width',
        str(width)
    ]

    if max_entries is not None:
        args.extend(['--max-entries', str(max_entries)])

    args.extend([
        'optimize-lpm-w-oi', '--oi-lpm-algo', 'incremental',
        '--oi-lpm-reduce-width-after', cls.path
    ])
    await _run_process_and_log(args)
    return lib.OptimizationResult(
        cls,
        path.join(RESULTS_DIR,
                  lib.METHOD_TEMPLATES['lpm_w_oi'].format(width=width)))


@cached(Global.get_cache, version=4)
async def generate_p4(opt_result_fut: Future[lib.OptimizationResult],
                      resources_fut: Optional[Future[Optional[lib.ResourceChoice]]],
                      p4_program: lib.P4Program, arch: lib.Arch,
                      bound_kind: lib.BoundKind, bound_value: int,
                      trans: lib.Transformation) -> lib.P4Program:
    opt_result = await opt_result_fut
    resources = await resources_fut if resources_fut is not None else None

    combine_lookups = 'true' if trans.combiner else 'false'
    resources_path = resources.path if resources is not None else ''

    result_path = path.join(opt_result.result_dir,
                            f'{bound_kind.old_name}_{bound_value}',
                            trans.key_method.name.lower(),
                            f'{trans.combiner}_combiner', arch.name.lower(),
                            'p4', f'{p4_program.basename}_transformed.p4')

    return await generate_p4_common(p4_program, result_path, [
        f'-DEXTRACT_KEYS={trans.key_method.extract_keys}',
        f'-DCLS_META=\\"{opt_result.meta_path}\\"',
        f'-DCOMBINE_LOOKUPS={combine_lookups}',
        f'-DRESOURCES=\\"{resources_path}\\"',
        bound_kind.compiler_options(bound_value)
    ])


@cached(Global.get_cache)
async def generate_p4_basic(opt_result_fut: Future[lib.OptimizationResult],
                            p4_program: lib.P4Program,
                            arch: lib.Arch) -> lib.P4Program:
    opt_result = await opt_result_fut

    total_size = _get_total_group_size(opt_result)

    result_path = path.join(opt_result.result_dir, 'basic', arch.name.lower(),
                            'p4', f'{p4_program.basename}_transformed.p4')

    return await generate_p4_common(p4_program, result_path,
                                    [f'-DSIZE={total_size}'])


@cached(Global.get_cache)
async def generate_p4_group(opt_result_fut: Future[lib.OptimizationResult],
                            p4_program: lib.P4Program, arch: lib.Arch,
                            group_idx: int) -> lib.P4Program:
    assert arch == p4_program.arch
    opt_result = await opt_result_fut
    meta = opt_result.load_meta()
    assert group_idx < len(meta['groups'])

    result_path = path.join(opt_result.result_dir, 'single_group',
                            str(group_idx), arch.name.lower(), 'p4',
                            f'{p4_program.basename}_transformed.p4')

    return await generate_p4_common(p4_program, result_path, [
        f'-DGROUP_IDX={group_idx}',
        f'-DCLS_META=\\"{opt_result.meta_path}\\"',
        f'-DEXTRACT_KEYS=true',
    ])


async def generate_p4_common(
        p4_program: lib.P4Program,
        result_path: str,
        extra_args: Sequence[str] = (),
) -> lib.P4Program:
    _makedirs(path.dirname(result_path), exist_ok=True)

    args = _common_predicat_args(p4_program, result_path) + list(extra_args)

    args = [lib.predicat_exe()] + args
    await _run_process_and_log(args)
    return lib.P4Program(result_path, p4_program.arch)


def _common_predicat_args(p4_program: lib.P4Program,
                          result_path: str) -> list[str]:
    return [
        f'-D__TARGET_TOFINO__={p4_program.arch.target_id}', '-I',
        lib.p4_include_path(), p4_program.path, '--outputPath', result_path
    ]


def _get_group_size(opt_result: lib.OptimizationResult, group: dict) -> int:
    with opt_result.open_group(group) as f:
        return _count_lines(f)


def _get_total_group_size(opt_result: lib.OptimizationResult) -> int:
    meta = opt_result.load_meta()
    total_size = 0
    for g in meta['groups']:
        total_size += _get_group_size(opt_result, g)
    with opt_result.open_group(meta['tcam']) as f:
        total_size += _count_lines(f)
    return total_size


def _count_lines(file: IO) -> int:
    return sum(1 for _ in file)


def _makedirs(dir_path, exist_ok: bool):
    if os.path.exists(dir_path):
        return
    task_logger().info(f'creating directory {dir_path}')
    if Global.dry_run:
        return
    os.makedirs(dir_path, exist_ok=exist_ok)


async def _run_process_and_log(args, **kwargs):
    async with Global.semaphore():
        extra_msg = 'cwd = ' + kwargs['cwd'] if 'cwd' in kwargs else ''
        message = 'running' + (f' ({extra_msg})' if extra_msg else '') + ':'
        task_logger().info(message + '\n' + ' '.join(args))
        return await _run_process(args, kwargs)


def task_logger() -> logging.Logger:
    task = asyncio.current_task()
    return logging.getLogger(task.get_name() if task else None)


def _touch_file(path: str):
    if Global.dry_run or Global.bootstrap:
        return
    with open(path, 'w'):
        pass


def _write_to_file(path: str, text: str):
    if Global.dry_run or Global.bootstrap:
        return
    with open(path, 'w') as f:
        f.write(text)


async def _run_process(args, kwargs):
    if Global.dry_run or Global.bootstrap:
        return
    timeout = kwargs.get('timeout')
    if 'timeout' in kwargs:
        del kwargs['timeout']
    process = await asyncio.create_subprocess_exec(*args, **kwargs)
    try:
        await asyncio.wait_for(process.wait(), timeout)
    except asyncio.TimeoutError:
        raise TimeoutExpired(args, timeout)
    returncode = cast(int, process.returncode)
    if returncode != 0:
        raise CalledProcessError(returncode, args)


if __name__ == '__main__':
    main()
