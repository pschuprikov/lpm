from __future__ import division

from collections import defaultdict
import csv

import click
import numpy as np
import matplotlib.pyplot as plt

def to_instance_name(fname, width, max_groups=None):
    if max_groups is None:
        return f'{fname}_{width}'
    else:
        return f'{fname}_{width}_{max_groups}'

def to_inc_instance_name(fname, width, max_groups):
    return f'{fname}_{width}_{max_groups}'

def get_ratio(data, kind, bt):
    if kind in data:
        return 100 * sum(data[kind]['groups'][:bt]) / data[kind]['total']
    else:
        return 0.0

def get_str(x):
    if x == 100.0:
        return '100'
    else:
        return '%.1f' % x

def get_inc_str(x):
    return f"{x['traditional']} & {x['recalcs']} & {x['in_lpm']}"

def collect_data(datatsv):
    plots = defaultdict(dict)
    results_reader = csv.reader(datatsv, delimiter='\t')
    for row in results_reader:
        kind, fname, width, max_groups, rest, groups = row[0], row[1], row[5], row[6], row[8], row[9]
        rest, groups = int(rest), eval(groups)
        max_groups = int(max_groups) if max_groups != 'None' else None

        if kind == 'oi_lpm_joint_exp':
            kind = kind + '_' + row[10]

        plots[to_instance_name(fname, width, max_groups)][kind] = {
            'total': sum(groups) + rest,
            'rest': rest,
            'groups': groups
        }
    return plots

def collect_inc_data(datatsv):
    plots = {}
    results_reader = csv.reader(datatsv, delimiter='\t')
    for row in results_reader:
        fname, total, width, max_groups, traditional, recalcs, in_lpm =\
                row[:4] + row[5:7] + row[8:]
        plots[to_inc_instance_name(fname, width, max_groups)] = {
            'total': total,
            'traditional': traditional,
            'recalcs': recalcs,
            'in_lpm': in_lpm
        }
    return plots

@click.command()
@click.option('--data', default='data.tsv', help='Where testing results are located')
@click.option('--inc-data', default='inc_data.tsv', 
              help='Where testing results for incremental updates are located')
@click.option('--tex', help='Output csv for LaTeX', is_flag=True)
@click.option('--dir', default='newgraphs', help='Output dir for LaTeX')
def test(data, inc_data, tex, dir):
    kinds = ['oi', 'oi_exact', 'oi_lpm_joint', 'oi_lpm_joint_exp']
    widths = ['16', '24', '32', '64', '104']
    files = ['acl1.txt', 'acl2.txt', 'acl3.txt', 'acl4.txt', 'acl5.txt'] + ['fw1.txt', 'fw2.txt', 'fw3.txt', 'fw4.txt', 'fw5.txt'] + ['ipc1.txt', 'ipc2.txt']

    with open(data, 'r', newline='') as datatsv:
        plots = collect_data(datatsv)
    with open(inc_data, 'r', newline='') as incdatatsv:
        inc_plots = collect_inc_data(incdatatsv)

    if tex:
        for fname in files:
            print('%s & %d & %s \\\\' % (
                fname[:-4], 
                plots[to_instance_name(fname, 16)]['oi_lpm_joint']['total'], 
                ' & '.join([
                    get_str(get_ratio(
                        plots[to_instance_name(
                            fname, width, None if width != widths[-1] else bt
                        )], 
                        'oi_lpm_joint' if width != widths[-1] else 'lpm_bounded', 
                        bt
                    ))
                    for width in widths for bt in [5, 10, 20] 
                ])
            ))

        for delta in [4, 8]:
            print('===') 
            for fname in files:
                kind = 'oi_lpm_joint_exp_%d' % delta
                print('%s & %d & %s \\\\' % (
                    fname[:-4], 
                    plots[to_instance_name(fname, 16)][kind]['total'], 
                    ' & '.join([
                        get_str(get_ratio(plots[to_instance_name(fname, width)],
                            kind, bt))
                        for width in [16, 24, 32] for bt in [5, 10, 20] 
                    ])
               ))

        print('===')
        for fname in files:
            fst_instance_name = to_inc_instance_name(fname, 32, 5)
            print(
                f"\\emph{{{fname[:-4]}}} & {inc_plots[fst_instance_name]['total']} & " 
                + ' & '.join(
                    get_inc_str(inc_plots[to_inc_instance_name(fname, 32, bt)])
                    for bt in [5, 10, 20]
                )
                + ' \\\\'
            )

    #colrs = ['r', 'g', 'b', 'm']
    for width in widths:
        for fname in files:
            if tex:
                for kind in ['oi', 'oi_lpm_joint', 'oi_lpm_joint_exp_8']:
                    if to_instance_name(fname, width) in plots and kind in plots[to_instance_name(fname, width)]:
                        data = plots[to_instance_name(fname, width)][kind]
                        with open(f'{dir}/{fname}.{kind}.{width}.csv', 'w') as outf:
                            outf.write('\n'.join([
                                f"{i + 1} {100 * sum(data['groups'][:i]) / data['total']}"
                                for i in range(20)
                            ]))
    #        else:
    #            plt.subplot(
    #                len(widths), len(files), f_idx * len(widths) + w_idx + 1
    #            )
    #            plt.title(to_instance_name(fname, width))
    #            plt.xlabel('Number of groups')
    #            plt.ylabel('Coverage, %')

    #            x = list(range(21))
    #            for k_idx, kind in enumerate(kinds):
    #                data = plots[to_instance_name(fname, width)][kind]
    #                y = [100 * sum(data['groups'][:i]) / data['total'] for i in range(21)]
    #                plt.plot(x, y, label=kind, color=colors[k_idx] , marker='o')
    #            plt.legend()
    #            plt.grid(True)
    #        # plt.subplot(
    #        #     len(widths), len(files), f_idx * len(widths) + w_idx + 1
    #        # )
    #        # plt.title(to_instance_name(fname, width))
    #        # plt.xlabel('Number of groups')
    #        # plt.ylabel('Coverage, %')

    #        # x = list(range(21))
    #        # for k_idx, kind in enumerate(kinds):
    #        #     data = plots[to_instance_name(fname, width)][kind]
    #        #     y = [100 * sum(data['groups'][:i]) / data['total'] for i in range(21)]
    #        #     plt.plot(x, y, label=kind, color=colors[k_idx] , marker='+')
    #        # plt.legend()
    #        # plt.grid(True)
    #plt.show()

if __name__ == '__main__':
    test()
