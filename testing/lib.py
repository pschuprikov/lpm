import hashlib
import itertools
import os
import json
import os.path as path
from enum import Enum, auto
from pickle import UnpicklingError, PicklingError
from typing import NamedTuple, NewType, IO, Optional
from dataclasses import dataclass

FULL_WIDTH = 104



class Variable(Enum):
    TCAM_ENTRIES = auto()
    STAGE_NUMBER = auto()


class Method(Enum):
    OI = auto()
    LPM_W_OI = auto()
    EXACT = auto()
    OI_LPM_JOINT = auto()
    BASIC = auto()


class HashedPath(NamedTuple):
    path: str

    def __getstate__(self):
        if not path.exists(self.path):
            raise PicklingError(f'path {self.path} does not exist')
        return self._compute_hash()

    def __setstate__(self, state):
        if not path.exists(self.path):
            raise UnpicklingError(f'path {self.path} does not exist')
        if self._compute_hash() != state:
            raise UnpicklingError()

    def _compute_hash(self):
        with open(self.path) as cls_file:
            return hashlib.sha256(cls_file.read().encode('utf-8')).digest()


class TimestampedPath(NamedTuple):
    path: str

    def __getstate__(self):
        if not path.exists(self.path):
            raise PicklingError(f'path {self.path} does not exist')
        return self._compute_timestamp()

    def __setstate__(self, state):
        if not path.exists(self.path):
            raise UnpicklingError(f'path {self.path} does not exist')
        if self._compute_timestamp() != state:
            raise UnpicklingError()

    def _compute_timestamp(self):
        return path.getmtime(self.path)


class HashedDirectory(NamedTuple):
    path: str

    def __getstate__(self):
        return self._compute_hash()

    def __setstate__(self, state):
        if self._compute_hash() != state:
            raise UnpicklingError()

    def _compute_hash(self):
        m = hashlib.sha256()
        for file in sorted(os.listdir(self.path)):
            if path.isdir(path.join(self.path, file)):
                continue
            with open(path.join(self.path, file)) as f:
                m.update(f.read().encode('utf-8'))
        return m.digest()


class Arch(Enum):
    TOFINO = auto()
    TOFINO2 = auto()

    @property
    def target_id(self) -> int:
        if self is Arch.TOFINO:
            return 1
        if self is Arch.TOFINO2:
            return 2
        raise AssertionError()

    @property
    def chip_type(self) -> int:
        if self is Arch.TOFINO:
            return 2
        if self is Arch.TOFINO2:
            return 5
        raise AssertionError()

    @property
    def p4_arch(self) -> str:
        if self is Arch.TOFINO:
            return 'tna'
        if self is Arch.TOFINO2:
            return 't2na'
        raise AssertionError()


class KeyMethod(Enum):
    KEY_SELECTOR = auto()
    KEY_EXTRACTOR = auto()

    @property
    def extract_keys(self) -> str:
        if self is KeyMethod.KEY_EXTRACTOR:
            return 'true'
        if self is KeyMethod.KEY_SELECTOR:
            return 'false'
        raise AssertionError(f'Unknown key_method: {self}')


class BoundKind(Enum):
    MAX_GROUPS = auto()
    PERCENTAGE = auto()

    def compiler_options(self, value: int) -> str:
        if self is BoundKind.MAX_GROUPS:
            return f'-DMAX_GROUPS={value}'
        if self is BoundKind.PERCENTAGE:
            return f'-DFRACTION={value}'
        raise AssertionError()

    @property
    def old_name(self) -> str:
        if self is BoundKind.MAX_GROUPS:
            return 'maxGroups'
        if self is BoundKind.PERCENTAGE:
            return 'fraction'
        raise AssertionError()


class Classifier(NamedTuple):
    name: str

    @property
    def path(self):
        return path.join('..', 'classbench-classifiers', 'expanded',
                         f'{self.name}.txt')

    def __getstate__(self):
        return HashedPath(self.path)

    def __setstate__(self, _):  # pyright: ignore
        pass


METHOD_TEMPLATES = {
    'oi': 'oi_act.{width}.ICNP_BLOCKERS',
    'lpm_w_oi': 'lpm_inc_act_postoi.None.{width}',
    'oi_lpm_joint': 'oi_lpm_joint_act.None.{width}',
    'exact': 'oi_exact_act.None.ICNP_BLOCKERS',
    'basic': 'id',
}


class Transformation(NamedTuple):
    bound_kind: BoundKind
    bound_val: int
    key_method: KeyMethod
    combiner: bool


class OptimizationResult(NamedTuple):
    cls: Classifier

    base_dir: str
    """Where every classifier's result should be stored"""

    @property
    def result_dir(self) -> str:
        return path.join(self.base_dir, f'{self.cls.name}.txt')

    @property
    def meta_path(self) -> str:
        return path.join(self.result_dir, 'meta.json')

    def load_meta(self) -> dict:
        with open(self.meta_path) as meta_f:
            return json.load(meta_f)

    def open_group(self, group: dict) -> IO:
        return open(self.get_group_path(group))

    def get_group_path(self, group: dict) -> str:
        return path.join(self.result_dir, group['path'])

    def __getstate__(self):
        return HashedDirectory(self.result_dir)

    def __setstate__(self, _):
        pass


@dataclass
class P4Program:
    path: str
    arch: Arch

    @property
    def basename(self):
        return path.splitext(path.basename(self.path))[0]

    def __getstate__(self):
        return HashedPath(self.path), self.__dict__

    def __setstate__(self, state):
        try:
            self.__dict__.update(state[1])
        except Exception as e:
            raise UnpicklingError(f'cannot unpickle P4Program: {e}') from e


@dataclass
class ConfiguredP4Program(P4Program):
    build_dir: str
    install_dir: Optional[str]

    @classmethod
    def from_p4_program(cls, program: P4Program, build_dir: str,
                        install_dir: Optional[str]):
        return cls(program.path, program.arch, build_dir, install_dir)

    @property
    def makefile(self):
        return path.join(self.build_dir, 'Makefile')

    def __getstate__(self):
        return TimestampedPath(self.makefile), super().__getstate__()

    def __setstate__(self, state):
        super().__setstate__(state[1])


@dataclass
class BuiltP4Program(ConfiguredP4Program):
    @classmethod
    def from_configured_p4_program(cls, program: ConfiguredP4Program):
        return cls.from_p4_program(program, program.build_dir,
                                   program.install_dir)

    @property
    def manifest(self) -> str:
        return path.join(self.build_dir, self.arch.name.lower(),
                         self.basename, 'manifest.json')

    @property
    def target_config(self) -> str:
        return path.join(self.build_dir, self.arch.name.lower(),
                         self.basename, f'{self.basename}.conf')

    def __getstate__(self):
        return TimestampedPath(self.manifest), super().__getstate__()

    def __setstate__(self, state):
        super().__setstate__(state[1])


class InstalledP4Program(BuiltP4Program):
    @classmethod
    def from_built_p4_program(cls, program: BuiltP4Program):
        return cls.from_configured_p4_program(program)


Analysis = NewType('Analysis', HashedPath)
Plot = NewType('Plot', str)


class TestResources(NamedTuple):
    number_partitions: int
    # number_subtrees_per_partition: int
    size: int


class ResourceChoice(NamedTuple):
    opt_result: OptimizationResult
    resources_per_group: list[TestResources]

    def persist(self):
        with open(self.path, 'w') as f:
            json.dump(self.resources_per_group, f)

    @property
    def path(self) -> str:
        return path.join(self.opt_result.result_dir, 'resources', 'resources.json')

    def __getstate__(self):
        return HashedPath(self.path)

    def __setstate__(self, _):  # pyright: ignore
        pass


class TestConfig(NamedTuple):
    width: int
    arch: Arch
    resources: TestResources

    @property
    def get_path(self) -> str:
        return path.join(
                'group_tests', 
                f'{self.width}_width', 
                f'{self.resources.number_partitions}_number_partitions', 
                f'{self.resources.size}_size')


def p4_include_path():
    bf_p4c_path = os.environ['BF_P4C_COMPILER']
    p4include = path.join(path.dirname(path.dirname(bf_p4c_path)), 'share',
                          'p4c', 'p4include')
    return p4include


def predicat_exe():
    return os.environ['PREDICAT_EXE']


def tofino_model_exe():
    return os.environ['TOFINO_MODEL_EXE']


def switchd_exe():
    return os.environ['SWITCHD_EXE']


def p4_predicat_dir():
    return path.abspath(path.join('..', 'p4_predicat'))


def create_nix_shell_args(
        flake_path: str, command: str,
        attr_path: str = '',
        str_args: Optional[dict[str, str]] = None,
        env: Optional[dict[str, str]] = None,
) -> list[str]:
    full_attr_path = '.devShells."x86_64-linux".default'
    if attr_path:
        full_attr_path += '.' + attr_path
    if str_args is None:
        str_args = {}
    if env is None:
        env = {}

    return ['nix-shell', '-E',
            f'(builtins.getFlake "{flake_path}"){full_attr_path}'
            ] + list(
        itertools.chain(*(('--argstr', n, v) for n, v in str_args.items()))) + [
               '--run', f'{_format_env(env)} {command}'
           ]


def _format_env(env: dict[str, str]) -> str:
    return ' '.join(f'{k}="{v}"' for k, v, in env.items())
